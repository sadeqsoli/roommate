﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UserDebug {
   
    public static string GetUserDebugger(int numb)
    {
        string s = "";
        switch (numb)
        {
            case 0:
                s = "09169090728";
                break;
            case 1:
                s = "09117775171";
                break;
            case 2:
                s = "09013474053";
                break;
        }
        return s;
    }
}
