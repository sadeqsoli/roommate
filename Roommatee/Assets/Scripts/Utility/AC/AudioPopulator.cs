﻿using UnityEngine;

public static class AudioPopulator 
{
    static int position = 0;
    static int samplerate = 44100;
    static float frequency = 440;

    public static AudioClip AudioClipCreate(string ClipName, int Length,int channel, int  frequency,  bool isStreaming)
    {
        AudioClip AudioClipToReturn = AudioClip.Create(ClipName, Length, channel, frequency, isStreaming, OnAudioRead, OnAudioSetPosition);
        return AudioClipToReturn;
    }
    public static void SetDataBody(AudioSource audioSource)
    {
        //AudioSource audioSource = GetComponent<AudioSource>();
        float[] samples = new float[audioSource.clip.samples * audioSource.clip.channels];
        audioSource.clip.GetData(samples, 0);

        for (int i = 0; i < samples.Length; ++i)
        {
            samples[i] = samples[i] * 0.5f;
        }
        audioSource.clip.SetData(samples, 0);
    }




    static void OnAudioRead(float[] data)
    {
        int count = 0;
        while (count < data.Length)
        {
            data[count] = Mathf.Sin(2 * Mathf.PI * frequency * position / samplerate);
            position++;
            count++;
        }
    }

    static void OnAudioSetPosition(int newPosition)
    {
        position = newPosition;
    }

    


}
