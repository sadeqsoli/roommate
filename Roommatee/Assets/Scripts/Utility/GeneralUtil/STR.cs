﻿using System;
using System.Text.RegularExpressions;

public static class STR
{
    const string MatchEmailPattern =
         @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
         + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
         + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
         + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    const string MatchPhoneNumberPattern = @"^09[0|1|2|3][0-9]{8}$";
         
    public static bool CheckPhoneNumberFormat(string phoneNumber)
    {
        if (phoneNumber != null)
        {
            return Regex.IsMatch(phoneNumber, MatchPhoneNumberPattern);
        }
        else
        {
            return false;
        }
    }
    public static bool CheckEmailFormat(string email)
    {
        if (email != null)
        {
            return Regex.IsMatch(email, MatchEmailPattern);
        }
        else
        {
            return false;
        }
       
    }

    public static bool VerifyVerificationCode(string verificationCodeInput, int charNumber)
    {
         if(verificationCodeInput.Length == charNumber)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool VerifyUsername(string verificationCodeInput, int charNumber)
    {
         if(verificationCodeInput.Length >= charNumber)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool IsTypingEnglish(string inputValue)
    {
       return Regex.IsMatch(inputValue, @"^[a-zA-Z0-9]*$");
    }
    public static string EraseIfIsNOTTypingEnglish(string inputValue)
    {
       return Regex.Replace(inputValue, @"[^a-zA-Z0-9 ]", "");
    }
    public static void SetResultToNull(string resualt)
    {
        resualt = "";
    }


    public static string RemoveMarks(string newInput)
    {
        return Replacer(newInput);
    }


    public static string Spacer(string input)
    {
        string a = input.Replace(" ", "");

        return a;
    }
    public static string RemoveComma(string input)
    {
        string a = input.Replace(",", " ");

        return a;
    }
    public static string Slasher(string input)
    {
        string a = input.Replace("\"", "");
        string b = a.Replace("/", "");

        return b;
    }
    static string Replacer(string input)
    {
        string a = input.Replace("\"", "");
        string b = a.Replace(",", "");
        string c = b.Replace("-", "");
        string d = c.Replace("'", "");
        string e = d.Replace("?", "");
        string f = e.Replace("!", "");
        string g = f.Replace(".", "");
        string h = g.Replace("/", "");
        string i = h.ToLower();

        return i;
    }
    public static bool IsMatchWith(string input, string pattern)
    {
        return Regex.Match(input, pattern).Success;
    }
    public static bool IsMatchPattern(string input, string pattern)
    {
        return Regex.IsMatch(input, pattern);
    }

    public static int IndexOfWholeWord(this string str, string word)
    {
        for (int j = 0; j < str.Length &&
            (j = str.IndexOf(word, j, StringComparison.OrdinalIgnoreCase)) >= 0; j++)
            if ((j == 0 || !char.IsLetterOrDigit(str, j - 1)) &&
                (j + word.Length == str.Length || !char.IsLetterOrDigit(str, j + word.Length)))
                return j;
        return -1;
    }



    public static bool Contains(this string source, string toCheck, StringComparison comp = StringComparison.OrdinalIgnoreCase)
    {
        return source.IndexOf(toCheck, comp) >= 0;
    }
    public static string Replace(this string source, string oldString,
                            string newString, StringComparison comparison)
    {
        int index = source.IndexOf(oldString, comparison);

        while (index > -1)
        {
            source = source.Remove(index, oldString.Length);
            source = source.Insert(index, newString);

            index = source.IndexOf(oldString, index + newString.Length, comparison);
        }

        return source;
    }




    //didn't used before
    public static string GetBetween(string strSource, string strStart, string strEnd)
    {
        const int kNotFound = -1;

        var startIdx = strSource.IndexOf(strStart);
        if (startIdx != kNotFound)
        {
            startIdx += strStart.Length;
            var endIdx = strSource.IndexOf(strEnd, startIdx);
            if (endIdx > startIdx)
            {
                return strSource.Substring(startIdx, endIdx - startIdx);
            }
        }
        return String.Empty;
    }

    //didn't used before
    public static string ReplaceTextBetween(string strSource, string strStart, string strEnd, string strReplace)
    {
        int Start, End, strSourceEnd;
        if (strSource.Contains(strStart) && strSource.Contains(strEnd))
        {
            Start = strSource.IndexOf(strStart, 0) + strStart.Length;
            End = strSource.IndexOf(strEnd, Start);
            strSourceEnd = strSource.Length - 1;

            string strToReplace = strSource.Substring(Start, End - Start);
            string newString = string.Concat(strSource.Substring(0, Start), strReplace, strSource.Substring(Start + strToReplace.Length, strSourceEnd - Start));
            return newString;
        }
        else
        {
            return string.Empty;
        }
    }




}//EndClassss
