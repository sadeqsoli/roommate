﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Clock
{
    #region Properties
    public static TimeSpan OneSecond { get { return oneSecond; } }
    #endregion

    #region Fields

    static TimeSpan oneSecond = new TimeSpan(1);
    #endregion
    #region Public Methods
    public static void Initialize()
    {
        DateTime initializeTime = DateTime.Now;
        DateTime DailyResetTime = initializeTime.Add(new TimeSpan(1, 0, 0, 0));
        TimeRepo.SetDailyInit_RestTime(initializeTime, DailyResetTime);
    }
    public static void SetCustomTimeSpan(TimeSpan timeSpan)
    {
        DateTime initializeTime = DateTime.Now;
        DateTime customResetTime = initializeTime.Add(timeSpan);
        TimeRepo.SetDailyInit_RestTime(initializeTime, customResetTime);
    }

    public static bool IsTimeToReset()
    {
        DateTime initializeTime = DateTime.Now;
        DateTime DailyResetTime = TimeRepo.GetDailyResetTime();

        if (DateTime.Now.Subtract(DailyResetTime) < new TimeSpan(0))
        {
            Debug.Log(DateTime.Now.Subtract(DailyResetTime).ToString());
            return true;
        }
        Debug.Log(DateTime.Now.Subtract(DailyResetTime).ToString());
        return false;
    }
    #endregion
    #region Private Methods
    #endregion
}
