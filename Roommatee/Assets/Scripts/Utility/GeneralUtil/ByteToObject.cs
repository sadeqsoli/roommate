﻿using UnityEngine;
using System.IO;
using System;
using ExitGames.Client.Photon;

public static class ByteToObject
{
    public static readonly byte[] memVector2 = new byte[2 * 4];
    public static byte[] SerializeVector2(object customobject)
    {
        float[] vo = (float[])customobject;
        MemoryStream ms = new MemoryStream(2 * 4);

        ms.Write(BitConverter.GetBytes(vo[0]), 0, 4);
        ms.Write(BitConverter.GetBytes(vo[1]), 0, 4);
        return ms.ToArray();
    }

    public static object DeserializeVector2(byte[] bytes)
    {
        float[] vo = new float[2];
        vo[0] = BitConverter.ToSingle(bytes, 0);
        vo[1] = BitConverter.ToSingle(bytes, 4);
        return vo;
    }

    public static short SerializeVector2Stream(StreamBuffer outStream, object customobject)
    {
        short[] vo = (short[])customobject;
        lock (memVector2)
        {
            byte[] bytes = memVector2;
            int index = 0;
            Protocol.Serialize(vo[0], bytes, ref index);
            Protocol.Serialize(vo[1], bytes, ref index);
            outStream.Write(bytes, 0, 2 * 4);
        }

        return 2 * 4;
    }
    public static object DeserializeVector2Stream(StreamBuffer inStream, short length)
    {
        short[] vo = new short[2];
        lock (memVector2)
        {
            inStream.Read(memVector2, 0, 2 * 4);
            int index = 0;
            Protocol.Deserialize(out vo[0], memVector2, ref index);
            Protocol.Deserialize(out vo[1], memVector2, ref index);
        }

        return vo;
    }
    public static void Initialize()
    {
        PhotonPeer.RegisterType(typeof(short), (byte)'W', SerializeVector2Stream, DeserializeVector2Stream);
        //Convert.ToBase64String(SerializeVector2("" as object));
    }



}//EndClassss
