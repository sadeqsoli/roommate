﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour
{
    Animator anim;


    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void SetTrigger(string newTrigger)
    {
        anim.SetTrigger(newTrigger);
    }

    void ResetTrigger(string newTrigger)
    {
        anim.ResetTrigger(newTrigger);
    }
}
