﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public enum FitterOrientation { Vertical, Horizontal, Uniform }
public class RectFitter : MonoBehaviour
{
    public FitterOrientation fitterOrientation;
    [Space]
    public RectTransform targetCanvas;
    [Space]
    public RectTransform targetObject;

    [Range(1, 100)]
    public float xDivider;
    [Space]
    [Range(1, 100)]
    public float yDivider;
    [Space]
    public float sumAmount = 0;
    [Space]
    public float paddingAmount = 0;


    void Start()
    {
        CalculateHeightOrWidth(fitterOrientation);
    }


    void CalculateHeightOrWidth(FitterOrientation fitterOrientation)
    {
        switch (fitterOrientation)
        {
            case FitterOrientation.Vertical:

                var y = targetCanvas.rect.height / yDivider;      // By Percentage
                targetObject.sizeDelta = new Vector2(targetObject.sizeDelta.x, y + sumAmount);
                break;

            case FitterOrientation.Horizontal:

                var x = targetCanvas.rect.width / xDivider;      // By Percentage
                targetObject.sizeDelta = new Vector2(x + sumAmount, targetObject.sizeDelta.y);
                break;

            case FitterOrientation.Uniform:

                // By Percentage in X and Y
                var xUniform = targetCanvas.rect.width / xDivider;
                var yUniform = targetCanvas.rect.height / yDivider; 
                targetObject.sizeDelta = new Vector2(xUniform + sumAmount, yUniform + sumAmount);
                break;
        }
    }

}//EndClassss
