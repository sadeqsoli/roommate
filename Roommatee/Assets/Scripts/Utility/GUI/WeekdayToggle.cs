﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class UnityEventWeekday : UnityEvent<bool, Weekday> { }

public class WeekdayToggle : MonoBehaviour
{

    public int IsStudyOn { get; private set; } = 0;

    public UnityEventWeekday WeekdayisOn = new UnityEventWeekday();
    [SerializeField] Weekday weekday;

    Toggle toggle;
    Animator animator;


    void Awake()
    {
        animator = GetComponent<Animator>();
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(SetButtonOFFAndON);
    }

    public void PreSettingForWeekdays(bool isOn)
    {
        IsStudyOn = isOn ? 1 : 0;
        animator?.SetBool("DayOFF", isOn);
        toggle.isOn = isOn;
        WeekdayisOn?.Invoke(isOn, weekday);
    }
    void SetButtonOFFAndON(bool isOn)
    {
        IsStudyOn = isOn ? 1 : 0;
        animator?.SetBool("DayOFF", isOn);

        WeekdayisOn?.Invoke(isOn, weekday);
    }


}
public enum Weekday { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday }
