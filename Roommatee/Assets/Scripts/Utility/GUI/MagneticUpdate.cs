﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticUpdate : MonoBehaviour
{
    const float fixedDelay = 0.000000000001f;
    public enum ObjectActive { FirstOn, FirstOff }
    [SerializeField] ObjectActive objectActive;
    [SerializeField] bool OnAwake = false;
    [SerializeField] float delayTime = fixedDelay;
    [SerializeField] bool isCustomTr = false;
    [SerializeField] Transform customTr = null;

    void Awake()
    {
        if (OnAwake)
            if (objectActive == ObjectActive.FirstOn)
            {
                StartCoroutine(SetChildsFirstON());
            }
            else
            {
                StartCoroutine(SetChildsFirstOFF());
            }
    }
    void OnEnable()
    {
        if (!OnAwake)
            if (objectActive == ObjectActive.FirstOn)
            {
                StartCoroutine(SetChildsFirstON());
            }
            else
            {
                StartCoroutine(SetChildsFirstOFF());
            }
    }



    IEnumerator SetChildsFirstON()
    {
        WaitForSeconds wait = new WaitForSeconds(delayTime);
        int childs = transform.childCount;
        if (childs > 0)
        {
            for (int i = 0; i < childs; i++)
            {
                if (isCustomTr)
                {
                    customTr.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    transform.GetChild(i).gameObject.SetActive(true);
                }
                yield return wait;
            }
            for (int i = 0; i < childs; i++)
            {
                if (isCustomTr)
                {
                    customTr.GetChild(i).gameObject.SetActive(false);
                }
                else
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                }
                yield return wait;
            }
        }
    }
    IEnumerator SetChildsFirstOFF()
    {
        WaitForSeconds wait = new WaitForSeconds(delayTime);
        int childs = transform.childCount;
        if (childs > 0)
        {
            for (int i = 0; i < childs; i++)
            {
                if (isCustomTr)
                {
                    customTr.GetChild(i).gameObject.SetActive(false);
                }
                else
                {
                    transform.GetChild(i).gameObject.SetActive(false);
                }
                yield return wait;
            }
            for (int i = 0; i < childs; i++)
            {
                if (isCustomTr)
                {
                    customTr.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    transform.GetChild(i).gameObject.SetActive(true);
                }
                yield return wait;
            }
        }
    }


}//EndClassss
