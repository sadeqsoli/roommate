﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonLongProcess : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    #region Fields
    bool IsRecordingDone = false;
    bool PressedRecordButton = false;
    float TimerAfterRecordButtonWasReleased;

    const float TimeAnalyzingUserSpeech = 2f;
    //[SerializeField] Image AnalyzeIMG ;
    [Space]
    [SerializeField] AudioVisualizer audioVisualizer;

    [Space]
    public UnityEvent OnPointerDownEvent;
    [Space]
    public UnityEvent OnPointerUpEvent;

    Animator anim;
    #endregion

    #region Public Methods
    public void OnPointerDown(PointerEventData eventData)
    {
        PressedRecordButton = true;
        IsRecordingDone = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (Microphone.devices[0] != null)
            Microphone.End(Microphone.devices[0]);
        audioVisualizer.StopVisualizing();
        IsRecordingDone = true;

    }
    #endregion

    #region Private Methods
    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        //When user Put the finger Down on the Button.
        OnPointerDownEvent?.AddListener(delegate
        {
            SetToRecordingAnim();
        });

        //When user lift the finger.
        OnPointerUpEvent?.AddListener(delegate
        {
            SetRecordingTransitionState();
        });
    }




    void SetToRecordingAnim()
    {
        anim.SetBool("recording", true);
        anim.SetBool("idle", false);
    }

    void SetRecordingTransitionState()
    {
        anim.SetBool("fromRecordingToIdle", true);
        anim.SetBool("recording", false);
    }

    //Event for Going Back to Idle From Recording.
    void SetFromRecordingToIdleAnim()
    {
        //audioVisualizer.enabled = true;
        anim.SetBool("idle", true);
        anim.SetBool("fromRecordingToIdle", false);
    }

    //Event for Going Back to Idle After Rotating.
    void SetToIdleAnim()
    {
        anim.SetBool("idle", true);
        anim.SetBool("recording", false);
    }


    private void Reset()
    {
        PressedRecordButton = false;
        IsRecordingDone = false;
        TimerAfterRecordButtonWasReleased = 0;
    }

    void WaitForSpeechAnalyzer()
    {
        if (IsRecordingDone)
        {
            TimerAfterRecordButtonWasReleased += Time.deltaTime;
            if (TimerAfterRecordButtonWasReleased >= TimeAnalyzingUserSpeech)
            {
                OnPointerUpEvent?.Invoke();
                Reset();
            }
        }
        else if (!IsRecordingDone && PressedRecordButton)
        {
            OnPointerDownEvent?.Invoke();
            Reset();
        }
    }


    void Update()
    {
        WaitForSpeechAnalyzer();
    }
    #endregion

}//EndCalssss