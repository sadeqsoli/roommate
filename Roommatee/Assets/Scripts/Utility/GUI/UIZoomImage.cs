﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class UIZoomImage : Static<UIZoomImage>
{
    #region Properties

    #endregion

    #region Fields
    [SerializeField] Image img;
    //this variables is for changing scale.
    const float minZoomScale = 1f;
    const float maxZoomScale = 1.5f;
    const float delayFadeIn_Out = 0.000000000000001f;
    float currZoomScale = minZoomScale;

    //scrollrect in Parent. 
    ScrollRect scrollRect;

    //Hold a Vector3 that is Zoom_OUT at its max Scale. 
    Vector3 initialScale = new Vector3(minZoomScale, minZoomScale, minZoomScale);

    //Hold a Vector3 that is Zoom_IN at its max Scale. 
    Vector3 maxScale = new Vector3(maxZoomScale, maxZoomScale, maxZoomScale);

    //Hold a Vector3 that is The Current Scale. 
    Vector3 currentScale;

    //Hold a Vector3 that is The Current Scale. 
    Vector2 initialAnchoredPos;




    //A constant value for delay in zoom process and do it smoothly.
    const float delay = 0.0000000001f;

    //A constant value for speed in zoom process.
    const float zoomSpeed = 0.03f;

    //A constant value for speed in Snapping process.
    const float snapSpeed = 7f;

    //for calculating x and y in Snapping process.
    float ySnapper, xSnapper;

    //for calculating x and y in Zooming process.
    float yZoomer, xZoomer;

    //An Array of float numbers that is for grading zoom modifier and smoothing the process.
    float[] zoomGrades = new float[12];

    //value of previous and current touch position.
    float touchesPrevPosDifference, touchesCurPosDifference;

    //position of first and second touch.
    Vector2 firstTouchPrevPos, secondTouchPrevPos;

    //position of first and second touch.
    bool onlyOnce = false, isMaxedOut = false, isMinedOut = false;
    #endregion
    #region Public Methods
    public void Snap_Out(GameObject obj)
    {
        StartCoroutine(FadeImage(obj, Zoom_Out, true));
    }
    public void Snap_In(GameObject obj)
    {
        StartCoroutine(FadeImage(obj, Zoom_In, false));
    }



    #endregion





    #region Private Methods
    void Start()
    {
        isMinedOut = false;
        isMaxedOut = false;
        onlyOnce = false;
        zoomGrades = SetZoomGrades();
        currentScale = initialScale;
        scrollRect = GetComponentInParent<ScrollRect>();
        initialAnchoredPos = GetRectTransform(gameObject).anchoredPosition;
    }


    RectTransform GetRectTransform(GameObject obj)
    {
        return obj.GetComponent<RectTransform>();
    }

    // Tap Zooming
    #region Tap Zoom IN _ OUT With Snapping IN _ OUT-----------------------------------------------
    void SnapTo_OUT(RectTransform target)
    {

        Canvas.ForceUpdateCanvases();
        ScrollRect scrollRect = this.GetComponentInParent<ScrollRect>();
        RectTransform rectTransform = GetRectTransform(gameObject);

        Vector2 anchoredPos = rectTransform.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(rectTransform.position)
        - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }

    void SnapTo_IN(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();
        ScrollRect scrollRect = this.GetComponentInParent<ScrollRect>();
        RectTransform mapTransform = GetRectTransform(gameObject);
        Vector2 anchoredPos = /*rectTransform.anchoredPosition =*/
            (Vector2)scrollRect.transform.InverseTransformPoint(mapTransform.position)
        - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);

        StartCoroutine(Snap_X(anchoredPos, mapTransform));
        StartCoroutine(Snap_Y(anchoredPos, mapTransform));
    }
    IEnumerator Snap_Y(Vector2 anchoredPos, RectTransform rectTransform)
    {
        float currY = rectTransform.anchoredPosition.y;
        if (anchoredPos.y > 0)
        {
            for (float j = currY; j < anchoredPos.y; j += snapSpeed)
            {
                ySnapper = j;
                rectTransform.anchoredPosition = new Vector2(xSnapper, j);
                yield return new WaitForSeconds(delay);
            }
        }
        else if (anchoredPos.y < 0)
        {
            for (float j = currY; j > anchoredPos.y; j -= snapSpeed)
            {
                ySnapper = j;
                rectTransform.anchoredPosition = new Vector2(xSnapper, j);
                yield return new WaitForSeconds(delay);
            }
        }

    }
    IEnumerator Snap_X(Vector2 anchoredPos, RectTransform rectTransform)
    {
        float currX = rectTransform.anchoredPosition.x;
        if (anchoredPos.x > 0)
        {
            for (float i = currX; i < anchoredPos.x; i += snapSpeed)
            {
                xSnapper = i;
                rectTransform.anchoredPosition = new Vector2(i, ySnapper);
                yield return new WaitForSeconds(delay);
            }
        }
        else if (anchoredPos.x < 0)
        {
            for (float i = currX; i > anchoredPos.x; i -= snapSpeed)
            {
                xSnapper = i;
                rectTransform.anchoredPosition = new Vector2(i, ySnapper);
                yield return new WaitForSeconds(delay);
            }
        }

    }
    IEnumerator Zoom_IN(GameObject obj)
    {
        if (transform.localScale == maxScale)
            yield break;

        float currentScale = transform.localScale.x;
        for (float i = currentScale; i < maxZoomScale; i += zoomSpeed)
        {
            transform.localScale = new Vector3(i, i, i);
            yield return new WaitForSeconds(delay);
        }
        transform.localScale = maxScale;
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo_IN(rectTransform);
    }
    IEnumerator Zome_OUT(GameObject obj)
    {
        if (transform.localScale == initialScale)
            yield break;

        float currentScale = transform.localScale.x;
        for (float i = currentScale; i > minZoomScale; i -= zoomSpeed)
        {
            transform.localScale = new Vector3(i, i, i);
            yield return new WaitForSeconds(delay);
        }
        transform.localScale = initialScale;

        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo_OUT(rectTransform);
        GetRectTransform(gameObject).anchoredPosition = initialAnchoredPos;
    }





    IEnumerator FadeImage(GameObject gameObj, UnityAction<GameObject> unityAction, bool isZoomingOut)
    {
        if(isZoomingOut == true)
        {
            if (isMinedOut == true)
                yield break;
        }
        else if (isZoomingOut == false)
        {
            if (isMaxedOut == true)
                yield break;
        }
            img.gameObject.SetActive(true);
        // fade from transparent to opaque
        // loop over 1 second
        for (float j = 0; j <= 1; j += Time.deltaTime * 2f)
        {
            if (j > 0.1)
            {
            }
            // set color with i as alpha
            img.color = new Color(1, 1, 1, j);
            yield return new WaitForSeconds(delayFadeIn_Out);
        }
        unityAction.Invoke(gameObj);
        // fade from opaque to transparent
        // loop over 1 second backwards
        for (float j = 1; j >= 0; j -= Time.deltaTime * 2f)
        {
            if (j < 0.1)
            {

            }
            // set color with i as alpha
            img.color = new Color(1, 1, 1, j);
            yield return new WaitForSeconds(delayFadeIn_Out);
        }
        img.gameObject.SetActive(false);
    }
    void SnapTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();
        ScrollRect scrollRect = this.GetComponentInParent<ScrollRect>();
        RectTransform rectTransform = this.GetComponent<RectTransform>();
        rectTransform.anchoredPosition =
        (Vector2)scrollRect.transform.InverseTransformPoint(rectTransform.position)
        - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }
    void Zoom_In(GameObject obj)
    {
        if (transform.localScale == maxScale)
            return;
        transform.localScale = maxScale;
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo(rectTransform);
    }
    void Zoom_Out(GameObject obj)
    {
        if (transform.localScale == initialScale)
            return;
        transform.localScale = initialScale;
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo(rectTransform);
    }

    #endregion


    //Zooming with two fingrs.
    #region Zoom IN _ OUT _ Finger----------------------------------------------
    void ScrollZoom_IN_OUT()//Zoom IN_OUT with ScrollBar.
    {
        if (transform.localScale.x < minZoomScale || transform.localScale.y < minZoomScale)
        {
            transform.localScale = initialScale;
        }
        if (transform.localScale.x > maxZoomScale || transform.localScale.y > maxZoomScale)
        {
            transform.localScale = maxScale;
        }
        Slider slider = this.GetComponent<Slider>();
        currZoomScale = slider.value;
        currentScale = new Vector3(currZoomScale, currZoomScale, currZoomScale);
        transform.localScale = currentScale;
    }


    IEnumerator MaxZoomEffect(bool zoomMaxedOut)
    {
        yield return new WaitForSeconds(0.5f);
        if (zoomMaxedOut)
        {
            float currentScale = transform.localScale.x;
            for (float i = currentScale; i > maxZoomScale; i -= Time.deltaTime)
            {
                transform.localScale = new Vector3(i, i, i);
                yield return new WaitForSeconds(delay);
            }
            transform.localScale = maxScale;
        }
        else
        {

            float currentScale = transform.localScale.x;
            for (float i = currentScale; i < minZoomScale; i += Time.deltaTime)
            {
                transform.localScale = new Vector3(i, i, i);
                yield return new WaitForSeconds(delay);
            }
            transform.localScale = initialScale;
        }
        isMaxedOut = false;
        isMinedOut = false;
    }
    void PinchZoom()//Zoom IN_OUT with Pinch IN and OUT.
    {
        if (Input.touchCount > 1)
        {
            if (Input.touchCount != 2)
                return;
            Touch firstTouch = Input.GetTouch(0);
            Touch secondTouch = Input.GetTouch(1);
            if (firstTouch.phase != TouchPhase.Moved && secondTouch.phase != TouchPhase.Moved)
                return;
            scrollRect.StopMovement();
            scrollRect.enabled = false;
            Vector2 firstTouchPos = Camera.main.ScreenToWorldPoint(firstTouch.position);
            Vector2 secondTouchPos = Camera.main.ScreenToWorldPoint(secondTouch.position);
            firstTouchPrevPos = firstTouchPos - firstTouch.deltaPosition;
            secondTouchPrevPos = secondTouchPos - secondTouch.deltaPosition;

            touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            touchesCurPosDifference = (firstTouchPos - secondTouchPos).magnitude;

            if (touchesPrevPosDifference > touchesCurPosDifference)
            {
                currZoomScale -= Time.deltaTime * 0.8f;
                if (IsInGrades(currZoomScale))
                    currentScale = new Vector3(currZoomScale, currZoomScale, currZoomScale);
                transform.localScale = currentScale;
                if (transform.localScale.x < 0.9f)
                {
                    currZoomScale = minZoomScale;
                    isMinedOut = true;
                    onlyOnce = true;
                    return;
                }
            }

            if (touchesPrevPosDifference < touchesCurPosDifference)
            {
                currZoomScale += Time.deltaTime * 0.8f;
                if (IsInGrades(currZoomScale))
                    currentScale = new Vector3(currZoomScale, currZoomScale, currZoomScale);
                transform.localScale = currentScale;
                if (transform.localScale.x > 1.6f)
                {
                    currZoomScale = maxZoomScale;
                    isMaxedOut = true;
                    onlyOnce = true;
                    return;
                }
            }

        }
        scrollRect.enabled = true;
    }

    float[] SetZoomGrades()//Setting ngrades for smoothing zooming process.
    {
        float[] grades = new float[12];
        grades[0] = 1f;
        for (int i = 0; i < grades.Length; i++)
        {
            grades[i] += 0.1f;
            if (i != grades.Length - 1)
            {
                grades[i + 1] = grades[i];
            }
        }
        return grades;
    }
    bool IsInGrades(float numb)//Is it in grades area?
    {
        for (int i = 0; i < zoomGrades.Length; i++)
        {
            if ((numb - zoomGrades[i]) < 0.01f)
            {
                return true;
            }
        }
        return false;
    }
    #endregion




    void LateUpdate()
    {
        if (isMinedOut == false && isMaxedOut == false && onlyOnce == false)
        {
            PinchZoom();
        }
        else if (isMinedOut && onlyOnce)
        {
            StartCoroutine(MaxZoomEffect(false));
            onlyOnce = false;
        }
        else if (isMaxedOut && onlyOnce)
        {
            StartCoroutine(MaxZoomEffect(true));
            onlyOnce = false;
        }
        if (transform.localScale == initialScale)
        {
            isMinedOut = true;
            isMaxedOut = false;
        }
        else if (transform.localScale == maxScale)
        {
            isMinedOut = false;
            isMaxedOut = true;
        }

    }//Updateeeee


    #endregion
}//EndClassss