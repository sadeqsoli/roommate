﻿/*
 * The intensity of the frequencies found between 0 and 44100 will be
 * grouped into 1024 elements. So each element will contain a range of about 43.06 Hz.
 * The average human voice spans from about 60 hz to 9k Hz
 * we need a way to assign a range to each object that gets animated. that would be the best way to control and modify animatoins.
*/
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class AudioVisualizer : MonoBehaviour
{
    [SerializeField] Transform[] audioSpectrumObjects;
    [Range(1, 100)] [SerializeField] float heightMultiplier;
    [Range(64, 8192)] [SerializeField] int numberOfSamples = 1024; //step by 2
    [SerializeField] FFTWindow fftWindow;
    [SerializeField] float lerpTime = 1;
    [SerializeField] AudioClip sampleClip;

    AudioSource audioSource;

    bool isSpecting = true;

    void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
        PlayMusic();
        isSpecting = true;
    }



    void PlayMusic()
    {
        audioSource.Stop();
        //Start recording to audioclip from the mic
        audioSource.loop = true;
        audioSource.clip = sampleClip;
        audioSource.Play();

    }

    public void StopVisualizing()
    {
        isSpecting = false;
        int Length = audioSpectrumObjects.Length;
        for (int i = 0; i < Length; i++)
        {
            audioSpectrumObjects[i].DOScaleY(Random.Range(3f, 10f), 0.5f).SetEase(Ease.InOutFlash);
            //audioSpectrumObjects[i].GetComponent<Image>().color = Tweener 
        }
        for (int i = 0; i < Length; i++)
        {
            audioSpectrumObjects[i].DOScaleY(1f, 0.1f).SetEase(Ease.InOutBounce);

            if (i == Length - 1)
                audioSpectrumObjects[i].DOScaleY(1f, 0.1f).SetEase(Ease.InOutBounce).onComplete += OnStop;
        }
    }

    void OnStop()
    {
        Debug.Log("End");
    }

    void GetSpectrum()
    {
        // initialize our float array
        float[] spectrum = new float[numberOfSamples];

        // populate array with fequency spectrum data
        audioSource.GetSpectrumData(spectrum, 0, fftWindow);


        // loop over audioSpectrumObjects and modify according to fequency spectrum data
        // this loop matches the Array element to an object on a One-to-One basis.
        for (int i = 0; i < audioSpectrumObjects.Length; i++)
        {

            // apply height multiplier to intensity
            float intensity = spectrum[i] * heightMultiplier;

            // calculate object's scale
            float lerpY = Mathf.Lerp(audioSpectrumObjects[i].localScale.y, intensity, lerpTime);
            Vector3 newScale = new Vector3(audioSpectrumObjects[i].localScale.x, lerpY, audioSpectrumObjects[i].localScale.z);
            //audioSpectrumObjects[i].DOScaleY(intensity, lerpTime);
            // appply new scale to object
            audioSpectrumObjects[i].localScale = newScale;

        }
    }

    void Update()
    {
        if (isSpecting)
            GetSpectrum();
    }

}
