﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


[RequireComponent(typeof(CanvasGroup))]
public class CharDraggable : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler
{
    #region Properties
    public Transform InitialPos { get { return initialPos; } set { initialPos = value; } }
    #endregion

    #region Fields
    RectTransform thisWordRectTranform;

    Transform initialPos;
    [SerializeField] List<GameObject> dropPlaces = new List<GameObject>();
    Transform initPosParent;
    Transform dragPlace;

    Transform currentDropPlace;
    #endregion

    #region Public Methods

    public void OnBeginDrag(PointerEventData eventData)
    {
        CheckForPlaceHolder(false);
        initialPos = this.transform.parent;
        this.transform.SetParent(initPosParent);

        this.transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f)
            .SetEase(Ease.InOutExpo);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.DOMove(initialPos.position, 0.5f, true)
            .SetEase(Ease.InExpo)
            .OnComplete(SetTransformAfterEndDrag);
    }

    void SetTransformAfterEndDrag()
    {
        transform.SetParent(initialPos);
        gameObject.transform.localScale = Vector3.one;
        gameObject.transform.localPosition = Vector3.zero;
        ChangeColorToNormal();
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        this.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.InOutExpo);
        CheckForPlaceHolder(true);
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (thisWordRectTranform.parent == dragPlace)
        {
            Debug.Log("sent by click!");
            currentDropPlace = SetOneAsFirst(dropPlaces);
            if (currentDropPlace != null)
            {
                thisWordRectTranform.DOMove(currentDropPlace.position, 0.25f, true)
                .SetEase(Ease.InOutExpo)
                .OnComplete(SetChildToDropZone);
            }
        }
        else if (IsCharOnDropPalce(thisWordRectTranform.parent))
        {
            int dragChildCount = dragPlace.childCount - 1;
            thisWordRectTranform.DOMove(dragPlace.GetChild(dragChildCount).position, 0.5f, true)
                .SetEase(Ease.InOutExpo)
                .OnComplete(SetChildToDragZone);
        }
        Debug.Log("Clicked!");
    }




    void SetChildToDropZone()
    {
        transform.SetParent(currentDropPlace);
        initialPos = currentDropPlace;
        this.transform.localScale = Vector3.one;
        this.transform.localPosition = Vector3.zero;
        Debug.Log("CurrentDropPlaceNOT: " + currentDropPlace.gameObject.name);
    }
    void SetChildToDragZone()
    {
        CharDroppable charDroppable = this.transform.parent.GetComponent<CharDroppable>();
        charDroppable.IsFilled = false;
        int NewChildNumber = dragPlace.childCount - 1;
        this.transform.SetParent(dragPlace);
        this.transform.SetSiblingIndex(NewChildNumber);
        this.transform.localScale = Vector3.one;
        this.transform.localPosition = Vector3.zero;
        //CheckForPlaceHolder(true);
    }




    #endregion


    #region Private Methods
    void Start()
    {
        thisWordRectTranform = (RectTransform)this.transform;
        initialPos = this.transform.parent;
        //getting drop places.
        GameObject[] dp = GameObject.FindGameObjectsWithTag("chardropzone");
        dropPlaces = PutArrayToList(dp);
        CheckSurroundings();
        //getting Drag place.
        initPosParent = GameObject.FindGameObjectWithTag("initPosParent").transform;
        dragPlace = GameObject.FindGameObjectWithTag("dragzone").transform;

    }//Startttttt


    void CheckForPlaceHolder(bool isTrue)
    {
        //Setting False Last Child in DragZone
        int DragChildCount = dragPlace.childCount;
        if (DragChildCount > 0)
            for (int i = DragChildCount - 1; i >= 0; i--)
            {
                if (dragPlace.GetChild(i).CompareTag("placeHolder"))
                {
                    dragPlace.GetChild(i).gameObject.SetActive(isTrue);
                    dragPlace.GetChild(i).SetAsLastSibling();
                    i = -1;
                    break;
                }
            }
    }

    void ChangeColorToNormal()
    {
        int Count = dropPlaces.Count;
        for (int i = 0; i < Count; i++)
        {
            if (dropPlaces[i] != null)
            {
                dropPlaces[i].GetComponent<Image>().color = _Color.DeselectColor;
            }
        }
    }



    void SetTransform(Transform CurrentTransform)
    {
        transform.SetParent(CurrentTransform);
        initialPos = CurrentTransform;
        this.transform.localScale = Vector3.one;
        this.transform.localPosition = Vector3.zero;
        Debug.Log("CurrentDropPlace: " + currentDropPlace.gameObject.name);
        //currentDropPlace = null;
    }

    Transform SetOneAsFirst(List<GameObject> gameObj)
    {
        Transform firstTransform = null;
        for (int i = 0; i < gameObj.Count; i++)
        {
            if (gameObj[i] != null)
            {
                CharDroppable charDroppable = gameObj[i].GetComponent<CharDroppable>();
                if (charDroppable.IsSelected == true && charDroppable.IsFilled == false)
                {
                    firstTransform = charDroppable.transform;
                    charDroppable.IsFilled = true;
                    charDroppable.IsSelected = false;
                    gameObj[i].GetComponent<Image>().color = _Color.DeselectColor;
                    //Debug.Log("Selected Type " + firstTransform.gameObject.name);
                    return firstTransform;
                }
            }
        }
        for (int k = 0; k < gameObj.Count; k++)
        {
            if (gameObj[k] != null)
            {
                CharDroppable charDroppable = gameObj[k].GetComponent<CharDroppable>();
                if (charDroppable.IsFilled == false)
                {
                    firstTransform = charDroppable.transform;
                    charDroppable.IsFilled = true;
                    gameObj[k].GetComponent<Image>().color = _Color.DeselectColor;
                    Debug.Log("Deselected Type " + firstTransform.gameObject.name);
                    return firstTransform;
                }
            }
        }
        return firstTransform;
    }

    bool IsCharOnDropPalce(Transform targetTransform)
    {
        CheckSurroundings();
        for (int i = 0; i < dropPlaces.Count; i++)
        {
            Transform currentTransform = dropPlaces[i].transform;
            if (currentTransform == targetTransform)
            {
                return true;
            }
        }
        return false;
    }


    List<GameObject> PutArrayToList(GameObject[] gameObjects)
    {
        List<GameObject> dpList = new List<GameObject>();
        for (int i = 0; i < gameObjects.Length; i++)
        {
            dpList.Add(gameObjects[i]);
        }
        return dpList;
    }
    void CheckSurroundings()
    {
        GameObject obj = null;
        for (int i = dropPlaces.Count - 1; i >= 0; i--)
        {
            if (dropPlaces[i] != null)
            {

            }
            else if (dropPlaces[i] == null)
            {
                dropPlaces.Add(obj);
                dropPlaces.RemoveAt(i);
            }
        }
    }




    int FindCloseDroppable(GameObject[] allDroppables)
    {
        for (int i = 0; i < allDroppables.Length; i++)
        {
            CharDroppable charDroppable = allDroppables[i].GetComponent<CharDroppable>();
            if (charDroppable.IsEntered == true)
            {
                return allDroppables[i].transform.GetSiblingIndex();
            }
        }
        return gameObject.transform.GetSiblingIndex();
    }
    bool IsCharOnDropPalce(Transform targetTransform, out int dropNumb)
    {
        CheckSurroundings();
        dropNumb = 0;
        for (int i = 0; i < dropPlaces.Count; i++)
        {
            Transform currentTransform = dropPlaces[i].transform;
            if (currentTransform == targetTransform)
            {
                dropNumb = i;
                return true;
            }
        }
        return false;
    }


    #endregion
}//EndClasssss
