﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class CharDroppable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler, IPointerClickHandler
{
    #region Properties
    public bool IsSelected { get; set; }
    public bool IsFilled { get { return isFilled; } set { isFilled = value; } }
    public bool IsEntered { get { return isEntered; } set { isEntered = value; } }
    #endregion

    #region Fields
    bool isFilled = false;
    bool isEntered = false;
    Color SelectedColor = _Color.Y_Olive;
    #endregion

    #region Public Methods
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
            return;
        gameObject.GetComponent<Image>().color = SelectedColor;

        CharDraggable charDraggable = eventData.pointerDrag.GetComponent<CharDraggable>();
        if (charDraggable != null)
        {
            isEntered = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
            return;
        gameObject.GetComponent<Image>().color = _Color.DeselectColor;
        CharDraggable charDraggable = eventData.pointerDrag.GetComponent<CharDraggable>();
        if (charDraggable != null)
        {
            isEntered = false;
            IsFilled = false;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        CharDraggable movingCharDraggable = eventData.pointerDrag.GetComponent<CharDraggable>();
        if (!GetDraggablesOnCurrentDropPlace())
        {
            if (movingCharDraggable != null && isFilled == false)
            {
                movingCharDraggable.InitialPos = this.transform;
                gameObject.GetComponent<Image>().color = _Color.DeselectColor;
                isFilled = true;
            }
        }
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        ObjectSelected();
    }

    #endregion


    #region Private Methods
    void Start()
    {
        IsSelected = false;
    }//Startttttt

    void ObjectSelected()
    {
        DeselectAllDroppables();
        IsSelected = !IsSelected;
        if (IsSelected)
        {
            gameObject.GetComponent<Image>().color = SelectedColor;
        }
        else
        {
            gameObject.GetComponent<Image>().color = _Color.DeselectColor;
        }
    }
    void DeselectAllDroppables()
    {
        GameObject[] dp = GameObject.FindGameObjectsWithTag("chardropzone");
        for (int i = 0; i < dp.Length; i++)
        {
            dp[i].GetComponent<CharDroppable>().IsSelected = false;
            dp[i].GetComponent<Image>().color = _Color.DeselectColor;
        }
    }


    bool GetDraggablesOnCurrentDropPlace()
    {
        CharDraggable currentDraggable = null;
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
        {
            GameObject currentChild = transform.GetChild(i).gameObject;
            if (currentChild.CompareTag("chardraggable") || currentChild.CompareTag("_chardraggable"))
            {
                currentDraggable = currentChild.GetComponent<CharDraggable>();
            }
        }
        if (currentDraggable != null)
        {
            isFilled = true;
            return true;
        }
        isFilled = false;
        return false;
    }


    #endregion
}//EndClasssss
