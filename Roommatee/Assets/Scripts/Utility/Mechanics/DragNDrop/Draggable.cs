﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class Draggable : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerClickHandler
{
    #region Properties

    public bool IsSelected { get { return isSelected; } set { isSelected = value; } }
    public Transform InitialPos { get { return initialPos; } set { initialPos = value; } }
    public Transform PlaceHolderParent { get { return placeholderParent; } set { placeholderParent = value; } }
    #endregion

    #region Fields

    RectTransform thisWordRectTranform;

    Transform initialPos;
    Transform placeholderParent;

    Transform dropPlace;
    Transform dragPlace;

    GameObject dragPlaceHolder = null;
    GameObject tapPlaceHolder = null;


    bool isSelected = false;

    #endregion

    #region Public Methods

    public void OnBeginDrag(PointerEventData eventData)
    {

        CheckForPlaceHolder(false);

        dragPlaceHolder = new GameObject();

        Image img = dragPlaceHolder.AddComponent<Image>();
        img.sprite = this.GetComponent<Image>().sprite;
        img.color = _Color.QuarterOpacityBlack;
        dragPlaceHolder.transform.SetParent(this.transform.parent);

        RectTransform thisWordRectTranform = (RectTransform)this.transform;

        LayoutElement le = dragPlaceHolder.AddComponent<LayoutElement>();
        le.preferredHeight = thisWordRectTranform.rect.height;
        le.preferredWidth = thisWordRectTranform.rect.width;

        dragPlaceHolder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());
        dragPlaceHolder.transform.localScale = Vector3.one;
        initialPos = this.transform.parent;
        placeholderParent = initialPos;
        this.transform.SetParent(initialPos.parent);
        GetComponent<CanvasGroup>().blocksRaycasts = false;

        this.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.5f);
    }
    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
        if (dragPlaceHolder.transform.parent != placeholderParent)
        {
            dragPlaceHolder.transform.SetParent(placeholderParent);
        }

        int newSiblingIndex = placeholderParent.childCount;

        for (int i = 0; i < placeholderParent.childCount; i++)
        {
            if (this.transform.position.x < placeholderParent.GetChild(i).position.x)
            {
                newSiblingIndex = i;

                if (dragPlaceHolder.transform.GetSiblingIndex() < newSiblingIndex)
                    newSiblingIndex--;
                break;
            }
        }
        dragPlaceHolder.transform.SetSiblingIndex(newSiblingIndex);
        dragPlaceHolder.transform.localScale = Vector3.one;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        dragPlaceHolder.transform.SetParent(initialPos);
        dragPlaceHolder.transform.localScale = Vector3.one;

        this.transform.DOMove(dragPlaceHolder.transform.position, 0.25f, true)
            .SetEase(Ease.InExpo)
            .OnComplete(SetTransformAfterEndDrag);
    }

    void SetTransformAfterEndDrag()
    {
        this.transform.SetParent(initialPos);
        this.transform.SetSiblingIndex(dragPlaceHolder.transform.GetSiblingIndex());
        this.transform.localScale = Vector3.one;
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        //this.transform.DORotate(Vector3.zero, 0.2f);
        this.transform.DOScale(Vector3.one, 0.2f);

        //Setting True Last Child in DropZone
        CheckForPlaceHolder(true);

        Debug.Log("EndDrag");
        Destroy(dragPlaceHolder);
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (thisWordRectTranform.parent == dragPlace)
        {
            int dropChildCount = dropPlace.childCount - 1;
            thisWordRectTranform.DOMove(dropPlace.GetChild(dropChildCount).position, 0.25f, true)
                .SetEase(Ease.InOutExpo)
                .OnComplete(SetChildToDropZone);
        }
        else if (thisWordRectTranform.parent == dropPlace)
        {
            int dragChildCount = dragPlace.childCount - 1;
            thisWordRectTranform.DOMove(dragPlace.GetChild(dragChildCount).position, 0.25f, true)
                .SetEase(Ease.InOutExpo)
                .OnComplete(SetChildToDragZone);
        }
    }

    void SetChildToDropZone()
    {
        int NewChildNumber = dropPlace.childCount - 1;
        this.transform.SetParent(dropPlace);
        this.transform.SetSiblingIndex(NewChildNumber);
        this.transform.localPosition = Vector3.zero;
        this.transform.localScale = Vector3.one;
        CheckForPlaceHolder(true);
    }
    void SetChildToDragZone()
    {
        int NewChildNumber = dragPlace.childCount - 1;
        this.transform.SetParent(dragPlace);
        this.transform.SetSiblingIndex(NewChildNumber);
        this.transform.localPosition = Vector3.zero;
        this.transform.localScale = Vector3.one;
        CheckForPlaceHolder(true);
    }





    void CheckForPlaceHolder(bool isTrue)
    {
        //Setting False Last Child in DropZone
        int DropChildCount = dropPlace.childCount;
        if (DropChildCount > 0)
            for (int i = DropChildCount - 1; i >= 0; i--)
            {
                if (dropPlace.GetChild(i).CompareTag("placeHolder"))
                {
                    dropPlace.GetChild(i).gameObject.SetActive(isTrue);
                    dropPlace.GetChild(i).SetAsLastSibling();
                    i = -1;
                }
            }

        //Setting False Last Child in DragZone
        int DragChildCount = dragPlace.childCount;
        if (DragChildCount > 0)
            for (int i = DragChildCount - 1; i >= 0; i--)
            {
                if (dragPlace.GetChild(i).CompareTag("placeHolder"))
                {
                    dragPlace.GetChild(i).gameObject.SetActive(isTrue);
                    dragPlace.GetChild(i).SetAsLastSibling();
                    i = -1;
                }
            }
    }
    #endregion


    #region Private Methods
    void Start()
    {
        thisWordRectTranform = (RectTransform)this.transform;
        initialPos = this.transform.parent;
        dropPlace = GameObject.FindGameObjectWithTag("dropzone").transform;
        dragPlace = GameObject.FindGameObjectWithTag("dragzone").transform;

    }//Startttttt



    #endregion
}//EndClasssss
