﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Droppable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
    #region Properties
    public bool IsFilled { get { return isFilled; } set { isFilled = value; } }
    public bool IsEntered { get { return isEntered; } set { isEntered = value; } }
    #endregion

    #region Fields
    bool isFilled = false;
    bool isEntered = false;
    Transform lastTR;
    #endregion

    #region Public Methods
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
            return;
        Draggable draggable = eventData.pointerDrag.GetComponent<Draggable>();
        if (draggable != null)
        {
            draggable.PlaceHolderParent = this.transform;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
            return;
        Draggable draggable = eventData.pointerDrag.GetComponent<Draggable>();
        if (draggable != null && draggable.PlaceHolderParent == this.transform)
        {
            draggable.PlaceHolderParent = draggable.InitialPos;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        Draggable draggable = eventData.pointerDrag.GetComponent<Draggable>();
        if (draggable != null)
        {
            draggable.InitialPos = this.transform;
        }
    }

    #endregion



}//EndClasssss
