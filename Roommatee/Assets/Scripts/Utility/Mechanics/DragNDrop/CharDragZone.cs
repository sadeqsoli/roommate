﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class CharDragZone : MonoBehaviour, /*IPointerEnterHandler, IPointerExitHandler,*/ IDropHandler
{
    #region Properties
    #endregion

    #region Fields
    Transform lastTR;
    #endregion

    #region Public Methods


    public void OnDrop(PointerEventData eventData)
    {
        CharDraggable charDraggable = eventData.pointerDrag.GetComponent<CharDraggable>();
        lastTR = charDraggable.InitialPos;
        CharDroppable charDroppable = lastTR.GetComponent<CharDroppable>();
        if (charDraggable != null && charDroppable != null)
        {
            Debug.Log("it's not null!");
            charDraggable.InitialPos = this.transform;
            charDroppable.IsFilled = false;
        }
    }

    #endregion



}//EndClasssss
