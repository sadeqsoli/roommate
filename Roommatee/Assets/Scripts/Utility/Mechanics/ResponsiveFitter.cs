﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ResponsiveFitter : MonoBehaviour
{
    [SerializeField] FitterOrientation fitterOrientation = FitterOrientation.Horizontal;

    [SerializeField] RectTransform _TargetParent;
    [SerializeField] RectTransform TargetChild;

    UnityEventString OnChangingTextValue = new UnityEventString();
    UnityEvent OnChangedValue = new UnityEvent();

    int strLength = 0;

    void Start()
    {
        TextMeshProUGUI textOBJ = TargetChild.GetComponent<TextMeshProUGUI>();
        strLength = textOBJ.text.Length;
        OnChangingTextValue?.AddListener(OnChangeValue);
        OnChangedValue?.AddListener(CheckForChange);
    }


    void CheckForChange()
    {
        if (fitterOrientation == FitterOrientation.Horizontal)
        {
            float childWidth = TargetChild.rect.width;
            _TargetParent.sizeDelta = new Vector2(childWidth, _TargetParent.sizeDelta.y); 
        }
        else if (fitterOrientation == FitterOrientation.Vertical)
        {
            float childHeight = TargetChild.rect.height;
            _TargetParent.sizeDelta = new Vector2(_TargetParent.sizeDelta.x, childHeight);
        }
    }

    void OnChangeValue(string str)
    {
        if(str.Length != strLength)
        {
            OnChangedValue?.Invoke();
        }
        strLength = str.Length;
    }



}//EndClassssss
