﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoRenderer : MonoBehaviour
{
    public RectTransform targetCanvas;
    public VideoPlayer videoPlayer;
    public RawImage rawImage;
    public TextMeshProUGUI text;
    public Button PlayButton;
    public Button PauseButton;
    public Button FastForwardButton;
    public Button FastBackwardButton;
    public Button RepeatButton;
    private int rawImageOriginWidth;
    private int rawImageOriginHeight;
    RectTransform rt;
    void Start()
    {
        rt = rawImage.GetComponent<RectTransform>();
        // get the rect size as base size for the upcomming video
        rawImageOriginWidth = Mathf.RoundToInt(rt.rect.width);
        rawImageOriginHeight = Mathf.RoundToInt(rt.rect.height);
        StartCoroutine(PlayVideo());
        AddListeners();
    }

    void AddListeners()
    {
        PlayButton.onClick.AddListener(PlayButtonAction);
        PauseButton.onClick.AddListener(PauseButtonAction);
        FastForwardButton.onClick.AddListener(ForwardButtonAction);
        FastBackwardButton.onClick.AddListener(BackwardButtonAction);
        RepeatButton.onClick.AddListener(RepeatButtonAction);
    }
    IEnumerator PlayVideo()
    {
        videoPlayer.url = Application.persistentDataPath + "/BrocaDB/Videos/TakingATaxi/L1/Video-TakingATaxi_L1.mp4";
        videoPlayer.playOnAwake = false;
        videoPlayer.Prepare();
        while (!videoPlayer.isPrepared)
        {
            Debug.Log("Preparing Video");
            yield return null;
        }
        rawImage.texture = videoPlayer.texture;
        int[] scaledVideo;
        if (videoPlayer.source == VideoSource.VideoClip)
        {
            scaledVideo = scaleResolution((int)videoPlayer.clip.width, (int)videoPlayer.clip.height, rawImageOriginWidth, rawImageOriginHeight);
        }
        else
        {
            Texture vidTex = videoPlayer.texture;
            scaledVideo = scaleResolution(vidTex.width, vidTex.height, rawImageOriginWidth, rawImageOriginHeight);
        }
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scaledVideo[0]);
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scaledVideo[1]);
        videoPlayer.Play();
    }

    int[] scaleResolution(int width, int heigth, int maxWidth, int maxHeight)
    {
        int new_width = width;
        int new_height = heigth;

        if (width > heigth)
        {
            new_width = maxWidth;
            new_height = (new_width * heigth) / width;
        }
        else
        {
            new_height = maxHeight;
            new_width = (new_height * width) / heigth;
        }

        int[] dimension = { new_width, new_height };
        return dimension;
    }

    void PlayButtonAction()
    {
        if (videoPlayer.isPaused)
        {
            videoPlayer.Play();
            SetCurrentTime();
        }
    }
    void PauseButtonAction()
    {
        if (videoPlayer.isPlaying)
        {
            videoPlayer.Pause();
            SetCurrentTime();
        }
    }
    void ForwardButtonAction()
    {
        videoPlayer.time += 10;
        videoPlayer.Play();
        SetCurrentTime();
    }
    void BackwardButtonAction()
    {
        videoPlayer.time -= 10;
        videoPlayer.Play();
        SetCurrentTime();
    }
    void RepeatButtonAction()
    {
        videoPlayer.time = 0;
        SetCurrentTime();
        StartCoroutine(PlayVideo());
    }

    void SetCurrentTime()
    {
        string currentMinutes = Mathf.Floor((int)videoPlayer.time / 60).ToString("00");
        string currentSeconds = ((int)videoPlayer.time % 60).ToString("00");

        string totalMinutes = Mathf.Floor((int)videoPlayer.length / 60).ToString("00");
        string totalSeconds = ((int)videoPlayer.length % 60).ToString("00");

        text.text = currentMinutes + " : " + currentSeconds + " | " + totalMinutes + " : " + totalSeconds;
    }

    void FindDeviceType()
    {
        var identifier = SystemInfo.deviceModel;
        if (identifier.Contains("tablet"))
        {

        }
        else if (identifier.Contains("phone"))
        {

        }
        else
        {

        }
    }


    private void Update()
    {
        if (videoPlayer.isPlaying)
            SetCurrentTime();
    }
    

}//EndClasssss