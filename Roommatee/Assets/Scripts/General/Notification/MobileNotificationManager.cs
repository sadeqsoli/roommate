﻿using UnityEngine;
using UnityEngine.UI;

public class MobileNotificationManager : MonoBehaviour
{
    #region public Variables
    //public Text txtGameStatus;
    #endregion

    #region Private Variables
    private const string gameTitle = "Broca";
    private const string sIcon = "app_icon_small";
    private const string lIcon = "app_icon_large";

    private string notifBody = "You've Left your App 1 minutes ago, try this new thing that we consider fun for for you!";
    private string gameStatus = "Opened from 1 Minutes Notification";

    #endregion

    #region Public Methods

    
    #endregion


    #region Private Methods
    void Start()
    {
        NotificationCenter.InitializeCheck();
        //txtGameStatus.text = NotificationCenter.PushingCustomDataNotification();

    }//Startttttt



    private void OnApplicationFocus(bool focus)
    {
        if (focus == false)
        {
            PushNotification(0, 1, 0);
        }
        else
        {
            NotificationCenter.InitializeCheck();
        }
    }

    private void PushNotification(int hours, int minutes, int seconds)
    {
        if (hours > 0 || minutes > 0 || seconds > 0)
        {
            NotificationCenter.SendNotification(gameTitle, notifBody, hours, minutes, seconds, sIcon, lIcon, gameStatus);
        }
    }
 


    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
