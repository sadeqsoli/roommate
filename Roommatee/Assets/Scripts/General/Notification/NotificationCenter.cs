﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class NotificationCenter 
{

    public static void SendNotification(string gameTitle , string notifBody ,int hours ,int minutes, int seconds, string smallIconName, string largeIconName, string gameStatus )
    {
        GleyNotifications.SendNotification(gameTitle, notifBody, new System.TimeSpan(hours, minutes, seconds), smallIconName, largeIconName, gameStatus);
    }

    public static void InitializeCheck()
    {
        GleyNotifications.Initialize();
    }

    public static string PushingCustomDataNotification()
    {
        return GleyNotifications.AppWasOpenFromNotification();
    }
}//EndClasssss
