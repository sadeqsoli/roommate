﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class AutomatedPrepare : MonoBehaviour
{
    private static string cbVersion = "1.0.0"; private static int cbVersionCode = 1;
    private static string gsVersion = "1.0.0"; private static int gsVersionCode = 1;
    private static string mkVersion = "1.0.0"; private static int mkVersionCode = 1;
    private static string iaVersion = "1.0.0"; private static int iaVersionCode = 1;
    private static string saVersion = "1.0.0";
    private static string asVersion = "1.0.0";


    #region menu items
    [MenuItem("Build/Build Mode/Debug Build")]
    public static void DebugBuild() { GameData.CurrentBuildMode = BuildMode.debug; }
    [MenuItem("Build/Build Mode/Open Beta Build")]
    public static void OpenBetaBuild() { GameData.CurrentBuildMode = BuildMode.openBeta; }
    [MenuItem("Build/Build Mode/Release Build")]
    public static void ReleaseBuild() { GameData.CurrentBuildMode = BuildMode.release; }


    [MenuItem("Build/Debug/Switch For Android Debugging")]
    public static void PrepareForAndroidDebugging()
    {
        GameData.CurrentBuildMode = BuildMode.debug;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "ir.sadeqsoli.a.de.Roommatee");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }


    [MenuItem("Build/Debug/Switch For iOS Debugging")]
    public static void PrepareForiOSDebugging()
    {
        GameData.CurrentBuildMode = BuildMode.debug;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "ir.sadeqsoli.iOS.de.Roommatee");
        SwitchPlatform(BuildTargetGroup.iOS, BuildTarget.iOS);
        SetSpecificiOS();
        SetAllowedOrientations();
    }






    [MenuItem("Build/Prepare For Build/Cafe Bazaar")]
    public static void PrepareForCafeBazaar()
    {
        PlayerSettings.bundleVersion = cbVersion;
        PlayerSettings.Android.bundleVersionCode = cbVersionCode;
        GameData.CurrentStoreForBuild = Store.CafeBazaar;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "ir.sadeqsoli.cb.Roommatee");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Prepare For Build/Google Store")]
    public static void PrepareForGoogleStore()
    {
        PlayerSettings.bundleVersion = gsVersion;
        PlayerSettings.Android.bundleVersionCode = gsVersionCode;
        GameData.CurrentStoreForBuild = Store.GoogleStore;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "ir.sadeqsoli.gs.Roommatee");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Prepare For Build/Myket")]
    public static void PrepareForMyket()
    {
        PlayerSettings.bundleVersion = mkVersion;
        PlayerSettings.Android.bundleVersionCode = mkVersionCode;
        GameData.CurrentStoreForBuild = Store.Myket;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "ir.sadeqsoli.mk.Roommatee");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Prepare For Build/IranApps")]
    public static void PrepareForIranApps()
    {
        PlayerSettings.bundleVersion = iaVersion;
        PlayerSettings.Android.bundleVersionCode = iaVersionCode;
        GameData.CurrentStoreForBuild = Store.IranApps;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "ir.sadeqsoli.ia.Roommatee");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Prepare For Build/Sib App")]
    public static void PrepareForSibApp()
    {
        PlayerSettings.bundleVersion = saVersion;
        GameData.CurrentStoreForBuild = Store.SibApp;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "ir.sadeqsoli.sa.Roommatee");
        SwitchPlatform(BuildTargetGroup.iOS, BuildTarget.iOS);
        SetSpecificiOS();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Prepare For Build/App Store")]
    public static void PrepareForAppStore()
    {
        PlayerSettings.bundleVersion = asVersion;
        GameData.CurrentStoreForBuild = Store.AppleStore;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "ir.sadeqsoli.as.Roommatee");
        SwitchPlatform(BuildTargetGroup.iOS, BuildTarget.iOS);
        SetSpecificiOS();
        SetAllowedOrientations();
    }

    #endregion

    #region Shared


    public static void SwitchPlatform(BuildTargetGroup targetGroup, BuildTarget target)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(targetGroup, target);
    }

    public static void SetAllowedOrientations()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
    }

    public static void SetAppIcon(string iconName, BuildTargetGroup targetGroup)
    {
        // icon name is address like this: assets/ddd/xxxx.jpg
        Texture2D icon = Resources.Load(iconName) as Texture2D;
        Texture2D[] icons = new Texture2D[] { icon, icon, icon, icon };
        PlayerSettings.SetIconsForTargetGroup(targetGroup, icons);

    }


    private static void SetSpecificAndroid()
    {
        //todo update all of these in right time
        GameData.CurrentPlatform = Platform.android;
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel19;
        PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevelAuto;
        PlayerSettings.Android.useCustomKeystore = false;
        PlayerSettings.Android.keystoreName = @"F:/OUTPUT/123456.keystore";// keystore name
        PlayerSettings.Android.keystorePass = "123456";// keystore password
        PlayerSettings.Android.keyaliasName = "123456";// keyalias name
        PlayerSettings.Android.keyaliasPass = "123456";// keyalias password
        SetScreenOrientation();
    }
  private static void SetSpecificiOS()
    {
        GameData.CurrentPlatform = Platform.ios;
        PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
        SetScreenOrientation();
    }

    private static void SetScreenOrientation() 
    {
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }



    #endregion

    #region Helper
    #endregion




}
