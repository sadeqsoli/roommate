﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
using UnityEngine.Video;

public class HostBundle : Singleton<HostBundle>
{
    #region public Variables
    #endregion

    #region Private Variables

    #endregion

    #region Public Methods

    public void Asset_Loader(string downloadUrl, string localUrl, string saveName)
    {
        if (!IsAssetExists(localUrl, saveName))
        {
            StartCoroutine(Download_Write_LoadAsset(downloadUrl, localUrl, saveName));
        }
        else
        {
            StartCoroutine(LoadAsset(localUrl, saveName));
        }
    }
    public void Scene_Loader(string downloadUrl, string localUrl, string saveName)
    {
        if (!IsAssetExists(localUrl, saveName))
        {
            StartCoroutine(Download_Write_LoadScene(downloadUrl, localUrl, saveName));
        }
        else
        {
            StartCoroutine(Load_Scene(localUrl, saveName));
        }
    }

    public void Delete(string localUrl, string saveName)
    {
        if (File.Exists(localUrl + saveName))
        {
            File.Delete(localUrl + saveName);
            Debug.Log("Deleted: " + localUrl + saveName);
            //txtstatus.text = "Deleted " + localUrl + saveName;
        }
        else
        {
            Debug.Log("Couldn't delete: " + localUrl + saveName);
            //txtstatus.text = "Cant delete " + localUrl + saveName;
        }
    }



    #endregion


    #region Private Methods


    IEnumerator LoadVideoAsset(string localUrl, string saveName)
    {
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle("file://" + localUrl + saveName);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            //txtstatus.text = request.error;
        }
        else
        {
            Debug.Log("Loading");
            //txtstatus.text = "Loading";
            AssetBundle asset = DownloadHandlerAssetBundle.GetContent(request);
            string[] All_Names = asset.GetAllAssetNames();
            foreach (string name in All_Names)
            {
                if (name != null)
                {
                    if (name == saveName)
                    {
                        VideoClip videoAsset = asset.LoadAsset<VideoClip>(saveName);
                        VideoPlayer player = FindObjectOfType<VideoPlayer>();
                        if (videoAsset != null)
                        {
                            player.clip = videoAsset;
                            player.Play();
                            Debug.Log("VideoName: " + name);
                            //txtstatus.text = "ObjectName= " + name;
                        }
                    }
                }

            }
        }
    }


    IEnumerator LoadAsset(string localUrl, string saveName)
    {
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle("file:///" + localUrl + saveName);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            //txtstatus.text = request.error;
        }
        else
        {
            Debug.Log("Loading");
            //txtstatus.text = "Loading";
            AssetBundle asset = DownloadHandlerAssetBundle.GetContent(request);
            string[] All_Names = asset.GetAllAssetNames();
            foreach (string name in All_Names)
            {
                if (name != null)
                {
                    GameObject gameAsset = (GameObject)asset.LoadAsset(name);
                    Instantiate(gameAsset);
                    Debug.Log("ObjectName: " + name);
                    //txtstatus.text = "ObjectName= " + name;
                }

            }
        }
        Debug.Log("Loaded");
        //txtstatus.text = "Loaded";
    }
    IEnumerator Load_Scene(string localUrl, string saveName)
    {
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle("file://" + localUrl + saveName);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            AssetBundle asset = DownloadHandlerAssetBundle.GetContent(request);
            string[] scenes = asset.GetAllScenePaths();
            foreach (string scenename in scenes)
            {
                Debug.Log("scene: " + scenename);
                Debug.Log("scene: " + saveName);
                SceneController.Instance.GoToSpecificScene(saveName);
                //txtstatus.text = "scene= " + scenename;
            }
        }
    }

    IEnumerator Download_Write_LoadScene(string downloadUrl, string localUrl, string saveName)
    {
        Debug.Log(downloadUrl);
        Debug.Log(localUrl + saveName);
        Debug.Log(saveName);
        UnityWebRequest request = UnityWebRequest.Get(downloadUrl);

        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            //Create Directory
            if (!Directory.Exists(localUrl))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
            }
            //Save Assets
            File.WriteAllBytes(localUrl + saveName, request.downloadHandler.data);
        }
        Debug.Log("Download");
        if (IsAssetExists(localUrl, saveName))
            yield return Load_Scene(localUrl, saveName);
    }
    IEnumerator Download_Write_LoadAsset(string downloadUrl, string localUrl, string saveName)
    {
        UnityWebRequest request = UnityWebRequest.Get(downloadUrl);

        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            //Create Directory
            if (!Directory.Exists(localUrl))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
            }
            //Save Assets
            File.WriteAllBytes(localUrl + saveName, request.downloadHandler.data);
        }
        Debug.Log("Download");
        if (IsAssetExists(localUrl, saveName))
            yield return LoadAsset(localUrl, saveName);
    }

    bool IsAssetExists(string localUrl, string saveName)
    {
        if (!File.Exists(localUrl + saveName))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    #endregion
}//EndClasssss
