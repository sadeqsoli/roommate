﻿using UnityEngine;

public static class StepRepo
{
    #region Properties
    public static string Daily_Steps_Repo { get { return REPO_STEPS_DAILY; } }
    public static string All_Steps_Repo { get { return REPO_STEPS_All; } }
    #endregion

    #region Fields
    const string REPO_STEPS_DAILY = "repo_Steps_Daily";
    const string REPO_STEPS_All = "repo_Steps_All";

    const string REPO_STEPS_Limit_DAILY = "repo_STEPS_Limit_DAILY";
    const string REPO_STEPS_Limit_PLOT = "repo_STEPS_Limit_PLOT";
    #endregion

    #region Public Methods----------------------------------

    #region Limit On Daily And Plot Steps
    public static void SetDailyLimitSteps(int newDailyLimitSteps)
    {
        //Saving new Daily Limit Steps.
        Save(REPO_STEPS_Limit_DAILY, newDailyLimitSteps);
    }
    public static int GetDailyLimitSteps()
    {
        int dailyLimitSteps;
        if (!PlayerPrefs.HasKey(REPO_STEPS_Limit_DAILY))
        {
            dailyLimitSteps = 0;
        }
        else
        {
            dailyLimitSteps = Retrive(REPO_STEPS_Limit_DAILY);

        }
        return dailyLimitSteps;
    }


    public static void SetPlotLimitSteps(int newPlotLimitSteps)
    {
        //Saving new Daily Limit Steps.
        Save(REPO_STEPS_Limit_PLOT, newPlotLimitSteps);
    }
    public static int GetPlotLimitSteps()
    {
        int plotLimitSteps;
        if (!PlayerPrefs.HasKey(REPO_STEPS_Limit_PLOT))
        {
            plotLimitSteps = 0;
        }
        else
        {
            plotLimitSteps = Retrive(REPO_STEPS_Limit_PLOT);

        }
        return plotLimitSteps;
    }

    #endregion

    #region Every Plot Steps
    public static void ResetPlotSteps(string targetPlotRepoKey)
    {
        Save(targetPlotRepoKey, 0);
    }
    public static void AddCurrentPlotSteps(string targetPlotRepoKey)
    {
        int dailyPlotSteps = Retrive(targetPlotRepoKey);
        dailyPlotSteps += 1;
        Save(targetPlotRepoKey, dailyPlotSteps);
    }
    public static void SetPlotSteps(int newPlotSteps, string targetPlotRepoKey)
    {
        //Saving All Players from 0 till present.
        int allCurrentPlotSteps = Retrive(targetPlotRepoKey);
        allCurrentPlotSteps += newPlotSteps;
        Save(targetPlotRepoKey, allCurrentPlotSteps);
    }
    public static int GetPlotSteps(string targetPlotRepoKey)
    {
        return Retrive(targetPlotRepoKey);
    }
    #endregion


    #region Daily Steps
    public static void ResetDailySteps()
    {
        //getting today's steps before reseting it.
        int dailySteps = GetDailySteps();
        //getting all steps.
        int allSteps = GetAllSteps();

        //adding today's steps to all steps.
        allSteps += dailySteps;

        //saving all steps after adding today's steps to it.
        Save(REPO_STEPS_All, allSteps);

        //reseting daily steps.
        Save(REPO_STEPS_DAILY, 0);
    }
    public static void AddDailySteps()
    {
        int dailyStep = Retrive(REPO_STEPS_DAILY);
        dailyStep += 1;
        Save(REPO_STEPS_DAILY, dailyStep);

    }
    public static void SetDailySteps(int targetSteps)
    {
        Save(REPO_STEPS_DAILY, targetSteps);
    }
    public static int GetDailySteps()
    {
        int dailySteps;
        if (!PlayerPrefs.HasKey(REPO_STEPS_DAILY))
        {
            dailySteps = 0;
        }
        else
        {
            dailySteps = Retrive(REPO_STEPS_DAILY);

        }
        return dailySteps;
    }
    #endregion




    #region All Steps

    public static int GetAllSteps()
    { 
        if (!PlayerPrefs.HasKey(REPO_STEPS_All))
        {
            return  0;
        }
        return Retrive(REPO_STEPS_All);
    }
    #endregion


    #endregion


    #region Private Methods

    static bool HasSteps(int reqCoins, string repoKey)
    {
        int steps = Retrive(repoKey);
        if (steps >= reqCoins)
        {
            return true;
        }
        return false;
    }





    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss



