﻿using UnityEngine;

public static class TaskPlotRepo 
{


    #region Public Methods
    public static void SetCurrentPlotTask(int enumNumb, string repoKey)
    {
        Save(repoKey, enumNumb);
    }
    public static int GetCurrent_Plot_Task(string repoKey)
    {
        return Retrive(repoKey);
    }
    #endregion


    #region Private Methods
    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss


public enum PlotTasks
{
    Plot,
    EditWordsAndSentences,
    Practice,
    Exam, 
    PlotTaskDone
}
