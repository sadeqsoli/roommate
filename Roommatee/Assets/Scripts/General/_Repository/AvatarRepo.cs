﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AvatarRepo 
{
    const string AvatarRepository = "PlayerAvatarRepo";
    public static void SetID(int enumNumb)
    {
        Save(AvatarRepository, enumNumb);
    }
    public static int GetID()
    {
        if (HasKey(AvatarRepository))
        {
            return Retrive(AvatarRepository);
        }
        return 0;
    }

    static bool HasKey(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return true;
        }
        return false;
    }
    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }

}//EndClassss
