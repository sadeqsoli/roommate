﻿using UnityEngine;


public static class PlayerLevelRepo 
{
    #region Properties
    public static string RepoPlayerLevel { get { return repoPlyerLevel; } }

    #endregion

    #region Fields
    const string repoPlyerLevel = "playerLevelRepo";
    #endregion

    #region Public Methods
    public static void Set(int enumNumb)
    {
        Save(repoPlyerLevel, enumNumb);
    }
    public static int Get()
    {
        return Retrive(repoPlyerLevel);
    }
    #endregion


    #region Private Methods
    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss
public enum PlayerLevel
{
    Beginner,
    Intermediate,
    Advanced
}