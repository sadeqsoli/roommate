﻿using UnityEngine;

public static class ScoreRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_Score = "scoresRepo";
    #endregion

    #region Public Methods
    public static void SetScore(int newScore)
    {
        int finalScore = Retrive(repo_Score);
        finalScore += newScore;

        Save(repo_Score, finalScore);
    }
    public static int GetScore()
    {
        return Retrive(repo_Score);
    }
    #endregion


    #region Private Methods
    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss


