﻿using UnityEngine;

public static class CoinRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_Coin = "coinsRepo";
    #endregion

    #region Public Methods
    public static void PopCoins(int reqCoins)
    {
        if (reqCoins <= 0)
            return;
        if (!HasCoin(reqCoins))
            return;
        int allCoins = Retrive(repo_Coin);
        allCoins -= reqCoins;

        Save(repo_Coin, allCoins);
    }
    public static void PushCoins(int newCoins)
    {
        int allCoins = Retrive(repo_Coin);
        allCoins += newCoins;

        Save(repo_Coin, allCoins);
    }
    public static int GetCoin()
    {
        return Retrive(repo_Coin);
    }
    #endregion


    #region Private Methods

    static bool HasCoin(int reqCoins)
    {
        int allCoins = Retrive(repo_Coin);
        if(allCoins >= reqCoins)
        {
            return true;
        }
        return false;
    }





    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss


