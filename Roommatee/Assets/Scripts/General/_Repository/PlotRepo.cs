﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PlotRepo
{
    #region Properties
    public const float DownloadWatchStage = 0f;
    public const float EditWS_Stage = 0.25f;
    public const float PracticeStage = 0.5f;
    public const float ExamStage = 0.75f;
    public const float PlotTaskDoneStage = 1f;
    #endregion

    #region Fields
    static string fieldSeprator = "/";
    const string IsPaidRep_L1 = "isPaidRepoLevelBeginner";
    const string IsPaidRep_L2 = "isPaidRepoLevelIntermediate";
    const string IsPaidRep_L3 = "isPaidRepoLevelExpert";


    const string AvailablePlotRepo_L1 = "AvailablePlotRepoLevelBeginner";
    const string AvailablePlotRepo_L2 = "AvailablePlotRepoLevelIntermediate";
    const string AvailablePlotRepo_L3 = "AvailablePlotRepoLevelAdvanced";

    const string repo_Plot = "plotsRepo";
    const string repo_TaskPlot = "TaskplotsRepo";
    #endregion

    #region Public Methods

    public static void SetCurrentPlot(int enumNumb)
    {
        SaveINT(repo_Plot, enumNumb);
    }
    public static int GetCurrentPlot()
    {
        return RetriveINT(repo_Plot);
    }




    public static Plots[] GetCurrentAvailablePlots()
    {
        PlayerLevel pLevel = (PlayerLevel)PlayerLevelRepo.Get();
        string[] openedPlots = GetOpenPlots(pLevel).Split('/');
        int openedPlotNumb = openedPlots.Length;
        Plots[] OpenedAvailablePlots = new Plots[openedPlotNumb];
        for (int i = 0; i < openedPlotNumb; i++)
        {
            Plots currntOpenPlot = ParseEnum<Plots>(openedPlots[i]);
            OpenedAvailablePlots[i] = currntOpenPlot;
        }
        //for sorting from min to max.
        Array.Sort(OpenedAvailablePlots);
        Debug.Log("Min to max: " + string.Join(",", OpenedAvailablePlots));

        //for sorting from max to min.
        Array.Reverse(OpenedAvailablePlots);
        Debug.Log("Max to min: " + string.Join(",", OpenedAvailablePlots));

        return OpenedAvailablePlots;
    }



    public static void SetProgressPerPlot(string repoKey, float progressAmount)
    {
        SaveFLOAT(repoKey, progressAmount);
    }
    public static float GetProgressPerPlot(string repoKey)
    {
        if (!PlayerPrefs.HasKey(repoKey))
            return 0f;
        return RetriveFLOAT(repoKey);
    }


    public static void SetPlotStagePP(string repoKey, int taskPlot)
    {
        SaveINT(repoKey, taskPlot);
    }
    public static int GetPlotStagePP(string repoKey)
    {
        if (!PlayerPrefs.HasKey(repoKey))
            return 0;
        return RetriveINT(repoKey);
    }



    public static void OpenPlot(Plots plot, PlayerLevel pLevel)
    {
        if (IsPlotLocked(plot, pLevel))
            return;
        switch (pLevel)
        {
            case PlayerLevel.Beginner:
                string OpenPlots_L1 = RetriveSTR(IsPaidRep_L1);
                OpenPlots_L1 += plot.ToString() + fieldSeprator;
                SaveSTR(IsPaidRep_L1, OpenPlots_L1);
                break;

            case PlayerLevel.Intermediate:
                string OpenPlots_L2 = RetriveSTR(IsPaidRep_L2);
                OpenPlots_L2 += plot.ToString() + fieldSeprator;
                SaveSTR(IsPaidRep_L2, OpenPlots_L2);
                break;

            case PlayerLevel.Advanced:
                string OpenPlots_L3 = RetriveSTR(IsPaidRep_L3);
                OpenPlots_L3 += plot.ToString() + fieldSeprator;
                SaveSTR(IsPaidRep_L3, OpenPlots_L3);
                break;

        }

    }
    public static string GetOpenPlots(PlayerLevel pLevel)
    {
        string openPlots = "";
        switch (pLevel)
        {
            case PlayerLevel.Beginner:
                if (!PlayerPrefs.HasKey(IsPaidRep_L1))
                {
                    SetTheFirstPlots(IsPaidRep_L1);
                }
                openPlots = RetriveSTR(IsPaidRep_L1);
                int lastSlash_L1 = openPlots.LastIndexOf(fieldSeprator);
                if (openPlots.Contains(fieldSeprator))
                {
                    openPlots = openPlots.Remove(lastSlash_L1);
                }
                //Debug.Log(openPlots);

                break;
            case PlayerLevel.Intermediate:
                if (!PlayerPrefs.HasKey(IsPaidRep_L2))
                {
                    SetTheFirstPlots(IsPaidRep_L2);
                }
                openPlots = RetriveSTR(IsPaidRep_L2);
                int lastSlash_L2 = openPlots.LastIndexOf(fieldSeprator);
                if (openPlots.Contains(fieldSeprator))
                {
                    openPlots = openPlots.Remove(lastSlash_L2);
                }
                //Debug.Log(openPlots);

                break;
            case PlayerLevel.Advanced:
                if (!PlayerPrefs.HasKey(IsPaidRep_L3))
                {
                    SetTheFirstPlots(IsPaidRep_L3);
                }
                openPlots = RetriveSTR(IsPaidRep_L3);
                int lastSlash_L3 = openPlots.LastIndexOf(fieldSeprator);
                if (openPlots.Contains(fieldSeprator))
                {
                    openPlots = openPlots.Remove(lastSlash_L3);
                }
                //Debug.Log(openPlots);

                break;
        }

        return openPlots;
    }
    public static bool IsPlotLocked(Plots plot, PlayerLevel pLevel)
    {
        string[] opendPlots = GetOpenPlots(pLevel).Split('/');
        int PlotLength = opendPlots.Length;
        for (int i = 0; i < PlotLength; i++)
        {
            if (plot.ToString().ToLower() == opendPlots[i].ToLower())
            {
                //Toast.Instance.SendToast(plot.ToString() + " is Opened Before! " + opendPlots[i]);
                return false;
            }
        }
        return true;
    }





    public static void PopPlot(Plots plot, PlayerLevel pLevel)
    {
        string oldPlot = plot.ToString();
        switch (pLevel)
        {
            case PlayerLevel.Beginner:
                List<string> availablePlots_L1 = GetAvailablePlotsList(pLevel);
                if (IsRepoHas(oldPlot, availablePlots_L1))
                    return;
                if (availablePlots_L1.Count == 3)
                {
                    DeleteFromRepo(oldPlot, AvailablePlotRepo_L1);
                }
                else
                {
                    Toast.Instance.SendToast("You have " + availablePlots_L1.Count + " available plots.");
                }
                break;

            case PlayerLevel.Intermediate:

                List<string> availablePlots_L2 = GetAvailablePlotsList(pLevel);
                if (IsRepoHas(oldPlot, availablePlots_L2))
                    return;
                if (availablePlots_L2.Count == 3)
                {
                    DeleteFromRepo(oldPlot, AvailablePlotRepo_L2);
                }
                else
                {
                    Toast.Instance.SendToast("You have " + availablePlots_L2.Count + " available plots.");
                }

                break;

            case PlayerLevel.Advanced:

                List<string> availablePlots_L3 = GetAvailablePlotsList(pLevel);
                if (IsRepoHas(oldPlot, availablePlots_L3))
                    return;
                if (availablePlots_L3.Count == 3)
                {

                    DeleteFromRepo(oldPlot, AvailablePlotRepo_L3);
                }
                else
                {
                    Toast.Instance.SendToast("You have " + availablePlots_L3.Count + " available plots.");
                }

                break;
        }
    }
    public static void PushPlot(Plots plot, PlayerLevel pLevel)
    {
        string newPlot = plot.ToString();
        switch (pLevel)
        {
            case PlayerLevel.Beginner:
                List<string> availablePlots_L1 = GetAvailablePlotsList(pLevel);
                if (IsRepoHas(newPlot, availablePlots_L1))
                    return;
                if (availablePlots_L1.Count < 3)
                {
                    availablePlots_L1.Add(newPlot);
                    string finalValue_L1 = string.Join(fieldSeprator, availablePlots_L1);
                    SaveSTR(AvailablePlotRepo_L1, finalValue_L1);
                }
                else
                {
                    Toast.Instance.SendToast("You should finish one of these " + availablePlots_L1.Count + " first.");
                }
                break;

            case PlayerLevel.Intermediate:

                List<string> availablePlots_L2 = GetAvailablePlotsList(pLevel);
                if (IsRepoHas(newPlot, availablePlots_L2))
                    return;
                if (availablePlots_L2.Count < 3)
                {
                    availablePlots_L2.Add(newPlot);
                    string finalValue_L2 = string.Join(fieldSeprator, availablePlots_L2);
                    SaveSTR(AvailablePlotRepo_L2, finalValue_L2);
                }
                else
                {
                    Toast.Instance.SendToast("You should finish one of these " + availablePlots_L2.Count + " first.");
                }

                break;

            case PlayerLevel.Advanced:

                List<string> availablePlots_L3 = GetAvailablePlotsList(pLevel);
                if (IsRepoHas(newPlot, availablePlots_L3))
                    return;
                if (availablePlots_L3.Count < 3)
                {
                    availablePlots_L3.Add(newPlot);
                    string finalValue_L3 = string.Join(fieldSeprator, availablePlots_L3);
                    SaveSTR(AvailablePlotRepo_L3, finalValue_L3);
                }
                else
                {
                    Toast.Instance.SendToast("You should finish one of these " + availablePlots_L3.Count + " first.");
                }

                break;
        }
    }
    public static string[] GetAvailablePlotsArray(PlayerLevel pLevel)
    {
        string[] availablePlots = null;
        switch (pLevel)
        {
            case PlayerLevel.Beginner:
                if (!PlayerPrefs.HasKey(AvailablePlotRepo_L1))
                {
                    SetTheFirstPlots(AvailablePlotRepo_L1);
                }
                string openPlots_l1 = RetriveSTR(AvailablePlotRepo_L1);
                int lastSlash_L1 = openPlots_l1.LastIndexOf(fieldSeprator);
                if (openPlots_l1.Contains(fieldSeprator))
                {
                    openPlots_l1 = openPlots_l1.Remove(lastSlash_L1);
                }
                availablePlots = openPlots_l1.Split('/');
                break;

            case PlayerLevel.Intermediate:

                if (!PlayerPrefs.HasKey(AvailablePlotRepo_L2))
                {
                    SetTheFirstPlots(AvailablePlotRepo_L2);
                }
                string openPlots_l2 = RetriveSTR(AvailablePlotRepo_L2);
                int lastSlash_L2 = openPlots_l2.LastIndexOf(fieldSeprator);
                if (openPlots_l2.Contains(fieldSeprator))
                {
                    openPlots_l2 = openPlots_l2.Remove(lastSlash_L2);
                }
                availablePlots = openPlots_l2.Split('/');

                break;

            case PlayerLevel.Advanced:

                if (!PlayerPrefs.HasKey(AvailablePlotRepo_L3))
                {
                    SetTheFirstPlots(AvailablePlotRepo_L3);
                }
                string openPlots_l3 = RetriveSTR(AvailablePlotRepo_L3);
                int lastSlash_L3 = openPlots_l3.LastIndexOf(fieldSeprator);
                if (openPlots_l3.Contains(fieldSeprator))
                {
                    openPlots_l3 = openPlots_l3.Remove(lastSlash_L3);
                }
                availablePlots = openPlots_l3.Split('/');

                break;
        }

        return availablePlots;
    }
    public static List<string> GetAvailablePlotsList(PlayerLevel pLevel)
    {
        List<string> availablePlots = new List<string>();
        switch (pLevel)
        {
            case PlayerLevel.Beginner:
                if (!PlayerPrefs.HasKey(AvailablePlotRepo_L1))
                {
                    string firstPlots = "";
                    firstPlots += Plots.TakingATaxi + fieldSeprator;
                    firstPlots += Plots.Restaurant + fieldSeprator;
                    firstPlots += Plots.ShoppingMall + fieldSeprator;

                    SaveSTR(AvailablePlotRepo_L1, firstPlots);
                }
                availablePlots = RetriveFromRepoToArray(AvailablePlotRepo_L1);

                break;

            case PlayerLevel.Intermediate:

                if (!PlayerPrefs.HasKey(AvailablePlotRepo_L2))
                {
                    string firstPlots = "";
                    firstPlots += Plots.TakingATaxi + fieldSeprator;
                    firstPlots += Plots.Restaurant + fieldSeprator;
                    firstPlots += Plots.ShoppingMall + fieldSeprator;

                    SaveSTR(AvailablePlotRepo_L2, firstPlots);
                }
                availablePlots = RetriveFromRepoToArray(AvailablePlotRepo_L2);

                break;

            case PlayerLevel.Advanced:

                if (!PlayerPrefs.HasKey(AvailablePlotRepo_L3))
                {
                    string firstPlots = "";
                    firstPlots += Plots.TakingATaxi + fieldSeprator;
                    firstPlots += Plots.Restaurant + fieldSeprator;
                    firstPlots += Plots.ShoppingMall + fieldSeprator;

                    SaveSTR(AvailablePlotRepo_L3, firstPlots);
                }
                availablePlots = RetriveFromRepoToArray(AvailablePlotRepo_L3);

                break;
        }

        return availablePlots;
    }

    #endregion





    #region Private Methods

    static List<string> RetriveFromRepoToArray(string repoKey)
    {
        string repoTarget = RetriveSTR(repoKey);
        return repoTarget.Split('/').ToList();
    }

    static bool IsRepoHas(string NewInput, List<string> availablePlots)
    {
        int availablePlotsNumb = availablePlots.Count;
        for (int i = 0; i < availablePlotsNumb; i++)
        {
            if (availablePlots[i] == NewInput)
            {
                return true;
            }
        }
        return false;
    }

    static void DeleteFromRepo(string oldPlot, string repoKey)
    {
        List<string> allPlots = RetriveFromRepoToArray(repoKey);
        int allWCount = allPlots.Count;

        for (int i = 0; i < allWCount; i++)
        {
            if (allPlots[i] == oldPlot)
            {
                allPlots[i] = "";
                string finalValue_AfterDel = string.Join(fieldSeprator, allPlots);
                if (!string.IsNullOrEmpty(finalValue_AfterDel))
                    SaveSTR(repoKey, finalValue_AfterDel);
            }
        }
    }


    static void SetTheFirstPlots(string repoKey)
    {
        string firstPlots = "";
        firstPlots += Plots.TakingATaxi + fieldSeprator;
        firstPlots += Plots.Restaurant + fieldSeprator;
        firstPlots += Plots.ShoppingMall + fieldSeprator;

        SaveSTR(repoKey, firstPlots);
    }






   public static T ParseEnum<T>(string value)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    static string RetriveSTR(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void SaveSTR(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }

    static float RetriveFLOAT(string key)
    {
        return PlayerPrefs.GetFloat(key);
    }
    static void SaveFLOAT(string key, float val)
    {
        PlayerPrefs.SetFloat(key, val);
    }
    static int RetriveINT(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void SaveINT(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion



}//EndClasssss

public enum PlotStage
{
    None, Started, Done
}

public enum Plots
{
    Airport,
    TakingATaxi,
    Hotel,
    Restaurant,
    ShoppingMall,
    OfficeBuildings,
    Mountain,
    AskingAndGivingDirection,
    Bank,
    GroceryShopping,
    Cafe,
    DoctorsBuilding,
    Pharmacy,
    Beach,
    Zoo,
    HouseRentalAgency,
    CarRentalAgency,
    GasStation,
    HouseholdChores,
    Gym
}

