﻿using UnityEngine;

public static class HistRepo
{
    #region Properties
    #endregion

    #region Fields

    #endregion

    #region Dialogue History
    public static void SetTryNumb(int tryoutNumber, PracticeDay practiceDay, string targetTryNumbRepoKey)
    {
        //TODO: NEED IMPROVMENT.

        //Saving All Players from 0 till present.
        int allCurrentTryouts = Retrive(targetTryNumbRepoKey);
        allCurrentTryouts += tryoutNumber;
        Save(targetTryNumbRepoKey, allCurrentTryouts);
    }
    public static void ResetTryoutNumb(PracticeDay practiceDay, string targetTryNumbRepoKey)
    {
        Save(targetTryNumbRepoKey, 0);
    }


    public static void AddTryNumb( PracticeDay practiceDay ,string targetTryNumbRepoKey)
    {
        if(practiceDay == PracticeDay.Day1)
        {
            string fullTargetRepoDay1 = targetTryNumbRepoKey + "Day1";
            //Saving All Players from 0 till present.
            int allCurrentTryouts = Retrive(fullTargetRepoDay1);
            allCurrentTryouts += 1;
            Save(fullTargetRepoDay1, allCurrentTryouts);
        }
        else if (practiceDay == PracticeDay.Day2)
        {
            string fullTargetRepoDay2 = targetTryNumbRepoKey + "Day2";
            //Saving All Players from 0 till present.
            int allCurrentTryouts = Retrive(fullTargetRepoDay2);
            allCurrentTryouts += 1;
            Save(fullTargetRepoDay2, allCurrentTryouts);
        }
        else if (practiceDay == PracticeDay.Day3)
        {
            string fullTargetRepoDay3 = targetTryNumbRepoKey + "Day3";
            //Saving All Players from 0 till present.
            int allCurrentTryouts = Retrive(fullTargetRepoDay3);
            allCurrentTryouts += 1;
            Save(fullTargetRepoDay3, allCurrentTryouts);
        }
        else if (practiceDay == PracticeDay.Day4)
        {
            string fullTargetRepoDay4 = targetTryNumbRepoKey + "Day4";
            //Saving All Players from 0 till present.
            int allCurrentTryouts = Retrive(fullTargetRepoDay4);
            allCurrentTryouts += 1;
            Save(fullTargetRepoDay4, allCurrentTryouts);
        }
        else if (practiceDay == PracticeDay.Day5)
        {
            string fullTargetRepoDay5 = targetTryNumbRepoKey + "Day5";
            //Saving All Players from 0 till present.
            int allCurrentTryouts = Retrive(fullTargetRepoDay5);
            allCurrentTryouts += 1;
            Save(fullTargetRepoDay5, allCurrentTryouts);
        }
        else if (practiceDay == PracticeDay.Day6)
        {
            string fullTargetRepoDay6 = targetTryNumbRepoKey + "Day6";
            //Saving All Players from 0 till present.
            int allCurrentTryouts = Retrive(fullTargetRepoDay6);
            allCurrentTryouts += 1;
            Save(fullTargetRepoDay6, allCurrentTryouts);
        }
    }
    public static int GetTryNumb(PracticeDay practiceDay, string targetTryNumbRepoKey)
    {
        if (practiceDay == PracticeDay.Day1)
        {
            //Saving this dialogue's history in practice day 1.
            string fullTargetRepoDay1 = targetTryNumbRepoKey + "Day1";
            int allCurrentTryoutsDay1 = Retrive(fullTargetRepoDay1);
            return allCurrentTryoutsDay1;
        }
        else if (practiceDay == PracticeDay.Day2)
        {
            //Saving this dialogue's history in practice day 2.
            string fullTargetRepoDay2 = targetTryNumbRepoKey + "Day2";
            int allCurrentTryoutsDay2 = Retrive(fullTargetRepoDay2);
            return allCurrentTryoutsDay2;
        }
        else if (practiceDay == PracticeDay.Day3)
        {
            //Saving this dialogue's history in practice day 3.
            string fullTargetRepoDay3 = targetTryNumbRepoKey + "Day3";
            int allCurrentTryoutsDay3 = Retrive(fullTargetRepoDay3);
            return allCurrentTryoutsDay3;
        }
        else if (practiceDay == PracticeDay.Day4)
        {
            //Saving this dialogue's history in practice day 4.
            string fullTargetRepoDay4 = targetTryNumbRepoKey + "Day4";
            int allCurrentTryoutsDay4 = Retrive(fullTargetRepoDay4);
            return allCurrentTryoutsDay4;
        }
        else if (practiceDay == PracticeDay.Day5)
        {
            //Saving this dialogue's history in practice day 5.
            string fullTargetRepoDay5 = targetTryNumbRepoKey + "Day5";
            int allCurrentTryoutsDay5 = Retrive(fullTargetRepoDay5);
            return allCurrentTryoutsDay5;
        }
        else if (practiceDay == PracticeDay.Day6)
        {
            //Saving this dialogue's history in practice day 6.
            string fullTargetRepoDay6 = targetTryNumbRepoKey + "Day6";
            int allCurrentTryoutsDay6 = Retrive(fullTargetRepoDay6);
            return allCurrentTryoutsDay6;
        }
            return -1;

    }
    #endregion


    #region Daily Steps



    #endregion


    #region Private Methods

    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss



