﻿using System;
using UnityEngine;

public static class TimeRepo
{
    #region Properties
    public static string Daily_Initialize_Time { get { return DAILY_Initialize_TIME; } }
    public static string Daily_Reset_Time { get { return DAILY_RESET_TIME; } }
    #endregion

    #region Fields

    const string PRACTICEDAYS_TOFINALPOINT = "PracticeDaysToFinalPointTime";

    const string FINAL_POINT_TIME = "FinalPointTime";
    const string TIMESPAN_TOFINALPOINT = "TimeSpanToFinalPointTime";

    const string DAILY_STUDY_TIME_LENGTH = "dialyStudyTimeLength";
    const string DAILY_STUDY_TIME = "dialyStudyTime";
    const string DAILY_WHAT_TIME = "WAHTDailyStudyTime";


    const string DAILY_RESET_TIME = "dialyResetTime";
    const string DAILY_Initialize_TIME = "dialyInitializeTime";
    #endregion

    #region Public Methods----------------------------------
    public static void SetWhatTime(DateTime givenTime)
    {
        string NewTimeSet = givenTime.ToBinary().ToString();

        Save(DAILY_STUDY_TIME, NewTimeSet);
    }
    public static DateTime GetWhatTime()
    {
        DateTime currentStudyTime;
        if (HasKey(DAILY_STUDY_TIME))
        {
            long timeAsBinary = Convert.ToInt64(Retrive(DAILY_STUDY_TIME));
            currentStudyTime = DateTime.FromBinary(timeAsBinary);
        }
        else
        {
            currentStudyTime = DateTime.Now;
        }

        return currentStudyTime;
    }

    public static void SetWhatTimeADay(TimeSpan givenTimeSpan)
    {
        Save(DAILY_WHAT_TIME, givenTimeSpan.Ticks.ToString());
    }
    public static TimeSpan GetWhatTimeADay()
    {
        string whatTimeInSTR = "";
        long ticks = 0;
        if (HasKey(DAILY_WHAT_TIME))
        {
            whatTimeInSTR = Retrive(DAILY_WHAT_TIME);
        }
        long.TryParse(whatTimeInSTR, out ticks);
        TimeSpan timeCounter = new TimeSpan(ticks);

        return timeCounter;
    }

    public static void SetFinalTime(DateTime givenDateTime)
    {

        string NewTimeSet = givenDateTime.ToBinary().ToString();

        Save(FINAL_POINT_TIME, NewTimeSet);

    }
    public static DateTime GetFinalTime()
    {
        DateTime currentStudyTime;
        if (HasKey(FINAL_POINT_TIME))
        {
            long timeAsBinary = Convert.ToInt64(Retrive(FINAL_POINT_TIME));
            currentStudyTime = DateTime.FromBinary(timeAsBinary);
        }
        else
        {
            currentStudyTime = DateTime.Now;
        }

        return currentStudyTime;
    }

    public static void SetMonthsToFinalPoint(TimeSpan givenTimeSpan)
    {
        int monthDifference = TimeSpanExtensions.GetMonths(givenTimeSpan);
        //Debug.Log(monthDifference);
        if (monthDifference > 0)
            SaveIntForTime(TIMESPAN_TOFINALPOINT, monthDifference);
    }
    public static int GetMonthsToFinalPoint()
    {
        if (HasKey(TIMESPAN_TOFINALPOINT))
        {
            return RetriveIntForTime(TIMESPAN_TOFINALPOINT);
        }
        else
        {
            return 0;
        }
    }



    public static void SetPracticeDaysToFinalPoint(int givendays)
    {
        if (givendays > 0)
            SaveIntForTime(PRACTICEDAYS_TOFINALPOINT, givendays);
    }
    public static int GetPracticeDaysToFinalPoint()
    {
        if (HasKey(PRACTICEDAYS_TOFINALPOINT))
        {
            return RetriveIntForTime(PRACTICEDAYS_TOFINALPOINT);
        }
        else
        {
            return 0;
        }
    }


    public static void SetHowMuch(int newHowMuch)
    {
        if (newHowMuch > 0)
        {
            int lastHowMuch = GetHowMuch();
            if (lastHowMuch != newHowMuch)
                SaveIntForTime(DAILY_STUDY_TIME_LENGTH, newHowMuch);
        }
    }
    public static int GetHowMuch()
    {
        if (HasKey(DAILY_STUDY_TIME_LENGTH))
        {
            return RetriveIntForTime(DAILY_STUDY_TIME_LENGTH);
        }
        else
        {
            return 5;
        }
    }





    #region Every Plot Steps
    public static void SetPlotNewResetTime(DateTime givenTime, string targetPlotRepoKey)
    {
        string NewTimeSet = givenTime.ToBinary().ToString();

        Save(targetPlotRepoKey, NewTimeSet);
    }
    public static DateTime GetCurrentPlotRestTime(string targetPlotRepoKey)
    {
        long timeAsBinary = Convert.ToInt64(Retrive(targetPlotRepoKey));
        DateTime currentPlotResetTime = DateTime.FromBinary(timeAsBinary);

        return currentPlotResetTime;
    }





    public static void SetDailyInit_RestTime(DateTime initTime, DateTime resetTime)
    {
        // Setting Initialize Time.
        string NewInitTime = initTime.ToBinary().ToString();
        Save(DAILY_Initialize_TIME, NewInitTime);

        //Setting Reset Time.
        string NewResetTime = resetTime.ToBinary().ToString();
        Save(DAILY_RESET_TIME, NewResetTime);

    }
    public static DateTime GetDailyInitializeTime()
    {
        long timeAsBinary = Convert.ToInt64(Retrive(DAILY_Initialize_TIME));
        DateTime currentInitializeTime = DateTime.FromBinary(timeAsBinary);

        return currentInitializeTime;
    }
    public static DateTime GetDailyResetTime()
    {
        DateTime currentResetTime = new DateTime(0);
        //if Clock is Initialized 
        if (HasKey(DAILY_RESET_TIME))
        {
            long timeAsBinary = Convert.ToInt64(Retrive(DAILY_RESET_TIME));
            currentResetTime = DateTime.FromBinary(timeAsBinary);
        }
        return currentResetTime;
    }


    #endregion





    #endregion


    #region Private Methods

    static bool HasTime(DateTime givenTime, string repoKey)
    {
        long dateTime = Convert.ToInt64(Retrive(repoKey));
        DateTime oldTime = DateTime.FromBinary(dateTime);
        if (givenTime.Subtract(oldTime) < Clock.OneSecond)
        {
            return true;
        }
        return false;
    }

    static bool HasKey(string repoKey)
    {
        if (PlayerPrefs.HasKey(repoKey))
        {
            return true;
        }
        return false;
    }



    static int RetriveIntForTime(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void SaveIntForTime(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }

    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }
    #endregion
}//EndClasssss



public static class TimeSpanExtensions
{
    public static int GetYears(this TimeSpan timespan)
    {
        return (int)(timespan.Days / 365.2425);
    }
    public static int GetMonths(this TimeSpan timespan)
    {
        return (int)(timespan.Days / 30.436875);
    }

}