﻿using UnityEngine;


public static class PDayRepo
{
    #region Properties
    #endregion

    #region Fields
    #endregion

    #region Public Methods
    public static bool SetProgress(PracticeDay progress, string repoKey)
    {
        int newProgress = SwitchPractice(progress);
        int currentProgress = Retrive(repoKey);
        if (currentProgress == 1 && newProgress == 0)
        {
            Save(repoKey, newProgress);
            return true;
        }
        else
        {
            if (currentProgress <= newProgress)
            {
                Save(repoKey, newProgress);
                return true;
            }
            else
            {
                Debug.Log("Couldn't Save, because " + currentProgress +" > " + newProgress);
                return false;
            }
        }
    }

    public static int GetProgress(string repoKey)
    {
        return Retrive(repoKey);
    }
    #endregion


    #region Private Methods

    static int SwitchPractice(PracticeDay progress)
    {
        int newProgress = 0;
        switch (progress)
        {
            case PracticeDay.None:
                newProgress = 0;
                break;
            case PracticeDay.Day1:
                newProgress = 1;
                break;
            case PracticeDay.Day2:
                newProgress = 2;
                break;
            case PracticeDay.Day3:
                newProgress = 3;
                break;
            case PracticeDay.Day4:
                newProgress = 4;
                break;
            case PracticeDay.Day5:
                newProgress = 5;
                break;
            case PracticeDay.Day6:
                newProgress = 6;
                break;
            case PracticeDay.Done:
                newProgress = 7;
                break;
        }
        return newProgress;
    }

    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss

public enum PracticeDay
{
    None,
    Day1,
    Day2,
    Day3,
    Day4,
    Day5,
    Day6,
    Done
}
