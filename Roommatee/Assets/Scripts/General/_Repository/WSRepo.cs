﻿using UnityEngine;


public static class WSRepo
{
    #region Public Variables

    #endregion

    #region Private Variables
    static string fieldSeprator = "/";
    static string commaSeprator = ",";

    #endregion

    #region Public Methods
    public static void PopBySTR(string oldInput, string repoKey)
    {
        if (!IsRepoHas(oldInput, repoKey))
            return;

        DeleteFromRepo(oldInput, repoKey);
    }
    public static void PushBySTR(string newInput, string repoKey)
    {
        if (IsRepoHas(newInput, repoKey))
            return;
        string value = Retrive(repoKey);
        value += newInput + fieldSeprator;
        Save(repoKey, value);
    }

    public static string Get(string repoKey)
    {
        string repoTarget = Retrive(repoKey);
        int lastSlashW = Retrive(repoKey).LastIndexOf(fieldSeprator);
        if (repoTarget.Contains(fieldSeprator))
        {
            repoTarget = Retrive(repoKey).Remove(lastSlashW);
        }

        return repoTarget;
    }


    public static void PopByIndex(int diaNumb, int wordNumb, string repoKey)
    {
        if (diaNumb < 0 || wordNumb < 0)
            return;
        DeleteByIndex(diaNumb, wordNumb, repoKey);
    }
    public static void PopByIndex(int diaNumb, string repoKey)
    {
        if (diaNumb < 0)
            return;
        DeleteByIndex(diaNumb, repoKey);
    }
    public static void PushByIndex(int diaNumb, int wordNumb, string repoKey)
    {
        string value = Retrive(repoKey);
        value += diaNumb + commaSeprator + wordNumb + fieldSeprator;
        Save(repoKey, value);
    }
    public static void PushByIndex(int diaNumb, string repoKey)
    {

        string value = Retrive(repoKey);
        value += diaNumb + fieldSeprator;
        Save(repoKey, value);
    }

    #endregion




    #region Private Methods

    static string[] RetriveFromRepoToArray(string repoKey)
    {
        string repoTarget = Retrive(repoKey);
        return repoTarget.Split('/');
    }

    static bool IsRepoHas(string NewInput, string repoKey)
    {
        string[] repoTarget = RetriveFromRepoToArray(repoKey);
        for (int i = 0; i < repoTarget.Length; i++)
        {
            if (repoTarget[i] == NewInput)
            {
                return true;
            }
        }
        return false;
    }

    static void DeleteFromRepo(string oldInput, string repoKey)
    {
        string[] Allwords = RetriveFromRepoToArray(repoKey);
        string value = Retrive(repoKey);
        for (int i = 0; i < Allwords.Length; i++)
        {
            if (Allwords[i] == oldInput)
            {
                Allwords[i] = "";
                value = ConvertToString(Allwords);
                if (!string.IsNullOrEmpty(value))
                    Save(repoKey, value);
            }
        }
    }
    static void DeleteByIndex(int diaNumb, int wordNumb, string repoKey)
    {
        string[] wordsIndex = RetriveFromRepoToArray(repoKey);
        string value = Retrive(repoKey);
        for (int i = 0; i < wordsIndex.Length; i++)
        {
            string targetIndex = diaNumb + commaSeprator + wordNumb;
            if (wordsIndex[i] == targetIndex)
            {
                wordsIndex[i] = "";
                value = ConvertToString(wordsIndex);
                if (!string.IsNullOrEmpty(value))
                    Save(repoKey, value);
            }
        }
    }
    static void DeleteByIndex(int diaNumb, string repoKey)
    {
        string[] wordsIndex = RetriveFromRepoToArray(repoKey);
        string value = Retrive(repoKey);
        for (int i = 0; i < wordsIndex.Length; i++)
        {
            if (wordsIndex[i] == diaNumb.ToString())
            {
                wordsIndex[i] = "";
                value = ConvertToString(wordsIndex);
                if (!string.IsNullOrEmpty(value))
                    Save(repoKey, value);
            }
        }
    }

    static string ConvertToString(string[] str)
    {
        string newS = "";
        for (int i = 0; i < str.Length; i++)
        {
            if (!string.IsNullOrEmpty(str[i]))
            {
                newS += str[i] + fieldSeprator;
            }
        }
        return newS;
    }



    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
        PlayerPrefs.Save();
    }


    #endregion
}//EndClasssss
