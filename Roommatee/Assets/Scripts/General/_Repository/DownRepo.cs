﻿using UnityEngine;


public static class DownRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_Download = "downloadedRepo";
    const string fieldSeprator = "/";
    #endregion

    #region Public Methods
    public static void SetDownloaded(string downloadedPlot)
    {
        if (IsRepoHas(downloadedPlot))
            return;
        string value = Retrive(repo_Download);
        value += downloadedPlot + fieldSeprator;
        Save(repo_Download, value);
    }
    public static bool IsRepoHas(string NewInput)
    {
        string[] repoTarget = RetriveFromRepoToArray();
        for (int i = 0; i < repoTarget.Length; i++)
        {
            if (repoTarget[i] == NewInput)
            {
                return true;
            }
        }
        return false;
    }
    public static string GetDownloded()
    {
        int lastSlashW = Retrive(repo_Download).LastIndexOf(fieldSeprator);
        string repoTarget = Retrive(repo_Download).Remove(lastSlashW);
        return repoTarget;
    }
    #endregion


    #region Private Methods


    static string[] RetriveFromRepoToArray()
    {
        string repoTarget = Retrive(repo_Download);
        return repoTarget.Split('/');
    }
    static void DeleteFromRepo(string oldInput)
    {
        string[] Allwords = RetriveFromRepoToArray();
        string value = Retrive(repo_Download);
        for (int i = 0; i < Allwords.Length; i++)
        {
            if (Allwords[i] == oldInput)
            {
                Allwords[i] = "";
                value = ConvertToString(Allwords);
                Save(repo_Download, value);
            }
        }
    }

    static string ConvertToString(string[] str)
    {
        string newS = "";
        for (int i = 0; i < str.Length; i++)
        {
            if (!string.IsNullOrEmpty(str[i]))
            {
                newS += str[i] + fieldSeprator;
            }
        }
        return newS;
    }






    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }
    #endregion
}//EndClasssss

public enum Download_Repo
{
    none,
    Taxi_L1,
    Taxi_L2,
    Taxi_L3,
    GasStation_L1,
    GasStation_L2,
    GasStation_L3,
    Restaurant_L1,
    Restaurant_L2,
    Restaurant_L3,
    Shopping_L1,
    Shopping_L2,
    Shopping_L3,
}
