﻿using UnityEngine;

public static class UserRepo
{
    #region Properties
    public static string RepoUserSignup { get { return repoUserSignup; } }
    public static string HaveSeenIntroduction { get { return "HaveSeenIntroduction"; } }

    #endregion

    #region Fields
    const string repoUserSignup = "userRepoSignup";
    const string repoUser = "userRepo";
    const string repoPhone = "phoneRepo";
    const string repoEmail = "emailRepo";
    const string repoAuth = "Authrepo";
    const string repoRefreshAuth = "RefreshAuthrpo";

    #endregion

    #region Public Methods


    public static void SetUserSignedIn(bool isTrue)
    {
        PlayerPrefs2.SetBool(repoUserSignup ,isTrue);
    }
    public static void PushUsername(string newUser)
    {
        Save(repoUser, newUser);
    }
    public static void PushPhone(string newPhone)
    {
        Save(repoPhone, newPhone);
    }
    public static void PushEmail(string newEmail)
    {
        Save(repoEmail, newEmail);
    }
    public static void PushAuth(string newAuth)
    {
        Save(repoAuth, newAuth);
    }
    public static void PushRefreshAuth(string newRefreshAuth)
    {
        Save(repoRefreshAuth, newRefreshAuth);
    }


    public static bool IsUserSignedIn()
    {
        return PlayerPrefs2.GetBool(repoUserSignup);
    }
    public static string GetUser()
    {
        return Retrive(repoUser);
    }
    public static string GetPhone()
    {
        return Retrive(repoPhone);
    }
    public static string GetEmail()
    {
        return Retrive(repoEmail);
    }
    public static string GetAuth()
    {
        return Retrive(repoAuth);
    }
    public static string GetRefreshAuth()
    {
        return Retrive(repoRefreshAuth);
    }

    #endregion




    #region Private Methods



    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }


    #endregion
}//EndClasssss
