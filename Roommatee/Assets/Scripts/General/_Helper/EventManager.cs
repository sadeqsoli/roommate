using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


public class UnityEventString : UnityEvent<string> { }
public class UnityEventBool : UnityEvent<bool> { }
public class UnityEventInt : UnityEvent<int> { }

public class EventManager : Singleton<EventManager>
{
    public static bool IsEvent_Initialized { get; private set; } = false;
    Dictionary<string, UnityEvent> eventDictionary = new Dictionary<string, UnityEvent>();
    Dictionary<string, UnityEvent<string>> stringEventDictionary = new Dictionary<string, UnityEvent<string>>();
    Dictionary<string, UnityEvent<bool>> boolEventDictionary = new Dictionary<string, UnityEvent<bool>>();
    Dictionary<string, UnityEvent<int>> intEventDictionary = new Dictionary<string, UnityEvent<int>>();

    protected override void Awake()
    {
        base.Awake();
        Initialize();
    }

    void Initialize()
    {
        if (eventDictionary == null)
            eventDictionary = new Dictionary<string, UnityEvent>();
        if (stringEventDictionary == null)
            stringEventDictionary = new Dictionary<string, UnityEvent<string>>();
        if (boolEventDictionary == null)
            boolEventDictionary = new Dictionary<string, UnityEvent<bool>>();
        if (boolEventDictionary == null)
            intEventDictionary = new Dictionary<string, UnityEvent<int>>();
        IsEvent_Initialized = true;
    }

   





    #region No Parameter Events
    public static void AddEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent> item in Instance.eventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent newEvent = new UnityEvent();
        Instance.eventDictionary.Add(eventName, newEvent);
    }
    public static void StartListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }
    public static void StopListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }
    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            if (thisEvent != null)
            {
                //Debug.Log(eventName + "'s Event is not null!");
                thisEvent.Invoke();
            }
            else
            {
                Debug.Log(eventName + "'s Event is null!");
            }
        }
    }
    #endregion




    #region Events with one Int parameter
    public static void AddIntEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent<int>> item in Instance.intEventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent<int> newEvent = new UnityEventInt();
        Instance.intEventDictionary.Add(eventName, newEvent);
    }
    public static void StartListening(string eventName, UnityAction<int> listener)
    {
        UnityEvent<int> thisEvent;
        if (Instance.intEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEventInt();
            thisEvent.AddListener(listener);
            Instance.intEventDictionary.Add(eventName, thisEvent);
        }
    }
    public static void StopListening(string eventName, UnityAction<int> listener)
    {
        UnityEvent<int> thisEvent;
        if (Instance.intEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }
    public static void TriggerEvent(string eventName, int targetNumber)
    {
        UnityEvent<int> thisEvent;
        if (Instance.intEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            if (thisEvent != null)
            {
                Debug.Log(eventName + "'s Event is not null!");
                thisEvent.Invoke(targetNumber);
            }
            else
            {
                Debug.Log(eventName + "'s Event is null!");
            }
        }
    }
    #endregion






    #region  Events with one String parameter
    public static void AddStringEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent<string>> item in Instance.stringEventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent<string> newEvent = new UnityEventString();
        Instance.stringEventDictionary.Add(eventName, newEvent);
    }
    public static void StartListening(string eventName, UnityAction<string> listener)
    {
        UnityEvent<string> thisEvent;
        if (Instance.stringEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEventString();
            thisEvent.AddListener(listener);
            Instance.stringEventDictionary.Add(eventName, thisEvent);
        }
    }
    public static void StopListening(string eventName, UnityAction<string> listener)
    {
        UnityEvent<string> thisEvent;
        if (Instance.stringEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }
    public static void TriggerEvent(string eventName, string targetString)
    {
        //Debug.Log(targetString);
        UnityEvent<string> thisEvent;
        if (Instance.stringEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent?.Invoke(targetString);
            //Debug.Log(eventName + "'s Event is not null!");
        }
    }
    #endregion



    #region  Events with one Bool parameter

    public static void AddBoolEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent<bool>> item in Instance.boolEventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent<bool> newEvent = new UnityEventBool();
        Instance.boolEventDictionary.Add(eventName, newEvent);
    }
    public static void StartListening(string eventName, UnityAction<bool> listener)
    {
        UnityEvent<bool> thisEvent;
        if (Instance.boolEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEventBool();
            thisEvent.AddListener(listener);
            Instance.boolEventDictionary.Add(eventName, thisEvent);
        }
    }
    public static void StopListening(string eventName, UnityAction<bool> listener)
    {
        UnityEvent<bool> thisEvent;
        if (Instance.boolEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }
    public static void TriggerEvent(string eventName, bool isTrue)
    {
        Debug.Log(isTrue);
        UnityEvent<bool> thisEvent;
        if (Instance.boolEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent?.Invoke(isTrue);
            Debug.Log(eventName + "'s Event is not null!");
        }
    }
    #endregion




}//EndClassss


public static class PEM
{
    //Events For Changing Listeners.
    public static string  L_Changer1 { get { return "L_Changer_Day1"; } }
    public static string  L_Changer2 { get { return "L_Changer_Day2"; } }
    public static string L_Changer3 { get { return "L_Changer_Day3"; } }
    public static string L_Changer4 { get { return "L_Changer_Day4"; } }
    public static string L_Changer5 { get { return "L_Changer_Day5"; } }
    public static string L_Changer6 { get { return "L_Changer_Day6"; } }



    //Events To Skip Next Practice.
    public static string  SKIP_WS1 { get { return "SKIP_WS_Day1"; } }
    public static string SKIP_WS2 { get { return "SKIP_WS_Day2"; } }
    public static string SKIP_WS3 { get { return "SKIP_WS_Day3"; } }
    public static string SKIP_WS4 { get { return "SKIP_WS_Day4"; } }
    public static string SKIP_WS5 { get { return "SKIP_WS_Day5"; } }
    public static string SKIP_WS6 { get { return "SKIP_WS_Day6"; } }

}

public static class PracticeEvent
{
    public static string  TC_D1 { get { return "TwoChoice_DayOne_Event"; } }
    public static string  AFR_D1 { get { return "AnswerFindRead_DayOne_Event"; } }
    public static string  TC_D2 { get { return "TwoChoice_DayTwo_Event"; } }
    public static string  EDP_D3 { get { return "EnglishDefinitionPicture_DayThree_Event"; } }
    public static string  SAD_D5 { get { return "SynonymAntonymDefinition_DayFive_Event"; } }
    public static string  LEDP_D6 { get { return "ListenEnglishDefinitionPicture_DaySix_Event"; } }
}
public static class SelectionEvent
{
    public static string  WordAddedEvent { get { return "AddingWord_Event"; } }
    public static string  WordRemovedEvent { get { return "RemovingWord_Event"; } }
    public static string  DialogueAddedEvent { get { return "AddingDialogue_Event"; } }
    public static string DialogueRemovedEvent { get { return "RemovingDialogue_Event"; } }
}

public static class PExam_StageEvent
{
    public static string QP1_BFC { get { return "QPackOne_B_FourChoices_Event"; } }
    public static string QP1_BR { get { return "QPackOne_B_Reading_Event"; } }
    public static string QP2_BU { get { return "QPackTwo_B_Unscramble_Event"; } }
    public static string QP2_BG { get { return "QPackTwo_B_Grammar_Event"; } }
    public static string QP2_IFC { get { return "QPackTwo_I_FourChoices_Event"; } }
    public static string QP1_IR { get { return "QPackOne_I_Reading_Event"; } }
    public static string QP2_IU { get { return "QPackTwo_I_Unscramble_Event"; } }
    public static string QP2_IG { get { return "QPackTwo_I_Grammar_Event"; } }
    public static string QP2_EU { get { return "QPackTwo_E_Unscramble_Event"; } }
    public static string QP2_EG { get { return "QPackTwo_E_Grammar_Event"; } }

}

public static class PExam_Event
{
    public static string TC_QPack1 { get { return "TC_QPackOne_Event"; } }
    public static string Gr_QPack2 { get { return "Gr_QPackTwo_Event"; } }

}