﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineManager : Singleton<CoroutineManager>
{
    public static bool IsCoroutines_Initialized { get; private set; } = false;
    private List<string> runningCoroutinesByStringName = new List<string>();
    private List<IEnumerator> runningCoroutinesByEnumerator = new List<IEnumerator>();


    protected override void Awake()
    {
        base.Awake();
        Initialize();
    }

    void Initialize()
    {
        if (runningCoroutinesByEnumerator == null)
            runningCoroutinesByEnumerator = new List<IEnumerator>();

        if (runningCoroutinesByStringName == null)
            runningCoroutinesByStringName = new List<string>();

        IsCoroutines_Initialized = true;
    }


    public Coroutine StartTrackedCoroutine(string methodName)
    {
        return StartCoroutine(GenericRoutine(methodName, null));
    }

    public Coroutine StartTrackedCoroutine(IEnumerator coroutine)
    {
        return StartCoroutine(GenericRoutine(coroutine));
    }

    public Coroutine StartTrackedCoroutine(string methodName, object parameter)
    {
        return StartCoroutine(GenericRoutine(methodName, parameter));
    }

    public bool IsTrackedCoroutineRunning(string methodName)
    {
        return Instance.runningCoroutinesByStringName.Contains(methodName);
    }

    public bool IsTrackedCoroutineRunning(IEnumerator coroutine)
    {
        return Instance.runningCoroutinesByEnumerator.Contains(coroutine);
    }

    public void StopTrackedCoroutine(string methodName)
    {
        if (!Instance.runningCoroutinesByStringName.Contains(methodName))
        {
            return;
        }
        StopCoroutine(methodName);
        Instance.runningCoroutinesByStringName.Remove(methodName);
    }

    public void StopTrackedCoroutine(IEnumerator coroutine)
    {
        if (!Instance.runningCoroutinesByEnumerator.Contains(coroutine))
        {
            return;
        }
        StopCoroutine(coroutine);
        Instance.runningCoroutinesByEnumerator.Remove(coroutine);
    }




    IEnumerator GenericRoutine(string methodName, object parameter)
    {
        Instance.runningCoroutinesByStringName.Add(methodName);
        if (parameter == null)
        {
            yield return StartCoroutine(methodName);
        }
        else
        {
            yield return StartCoroutine(methodName, parameter);
        }
        Instance.runningCoroutinesByStringName.Remove(methodName);


    }

    IEnumerator GenericRoutine(IEnumerator coroutine)
    {
        Instance.runningCoroutinesByEnumerator.Add(coroutine);

        yield return StartCoroutine(coroutine);
        Instance.runningCoroutinesByEnumerator.Remove(coroutine);
    }

}
