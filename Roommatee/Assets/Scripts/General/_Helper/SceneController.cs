﻿using DG.Tweening;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : Singleton<SceneController>
{
    [SerializeField] Image BackgroundIMG;
    [SerializeField] Image ProgressIMG;
    [SerializeField] TextMeshProUGUI ProgressTXT;
    int TapCount = 0;
    float NewTime = 0;
    float MaxDubbleTapTime = 0.3f;
    public UnityAction escapeButtonAction;

    public void GoToNextOrPrevScene(bool isGoingNext)
    {
        int buildIndex = SceneManager.GetActiveScene().buildIndex;
        int buildNumber;
        if (isGoingNext)
        {
            buildNumber = buildIndex + 1;
        }
        else
        {
            buildNumber = buildIndex - 1;
        }
        StartCoroutine(LoadScene(buildNumber));
    }
    public void GoToSpecificScene(int sceneIndex)
    {
        StartCoroutine(LoadScene(sceneIndex));
    }
    public void GoToSpecificScene(string sceneName, bool isSplash = false)
    {
        StartCoroutine(LoadScene(sceneName, isSplash));
    }
    public void ResetScene()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        StartCoroutine(LoadScene(sceneIndex));
    }
    public void QuitByEscapeButton()
    {
        // Check if Back was pressed this frame
        if (EscapeButtonWasHit())
        {
            TapCount += 1;
            if (TapCount == 1)
            {
                NewTime = Time.time + MaxDubbleTapTime;
                Toast.Instance.SendToast("Tab twice for quiting!");
                if (NewTime >= Time.time)
                {
                    TapCount = 0;
                    NewTime = 0;

                }
            }
            else if (TapCount == 2 && NewTime < Time.time)
            {
                // Quit the application
                NewTime = 0;
                TapCount = 0;
                Toast.Instance.SendToast("Quit!");
                Application.Quit(0);
            }
        }

    }

    void Start()
    {
        BackgroundIMG.gameObject.SetActive(false);
    }
    void GoBackByEscapeButton()
    {
        // Check if Back was pressed this frame
        if (EscapeButtonWasHit())
        {
            escapeButtonAction?.Invoke();
        }
    }

    bool EscapeButtonWasHit()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                return true;
            }
        }
        return false;
    }


    IEnumerator LoadScene(string sceneName, bool isSplash = false)
    {

        if (!isSplash)
        {
            ProgressIMG.fillAmount = 0f;
            ProgressTXT.text = "%0";
            float fadeTime = 1f;
            float delay = 1.5f;

            BackgroundIMG.gameObject.transform.localScale = Vector3.zero;
            BackgroundIMG.gameObject.SetActive(true);
            BackgroundIMG.gameObject.transform.DOScale(Vector3.one, fadeTime)
                .SetEase(Ease.InOutExpo);

            yield return new WaitForSecondsRealtime(delay);
            AsyncOperation asyncLoader = SceneManager.LoadSceneAsync(sceneName);

            ProgressTXT.DOColor(_Color.G_DoneGreen, fadeTime);

            while (!asyncLoader.isDone)
            {
                float progress = Mathf.Clamp01(asyncLoader.progress / 0.9f);
                ProgressIMG.fillAmount = progress;
                ProgressTXT.text = "% " + (progress * 100).ToString("###");
                yield return null;
            }
            yield return new WaitForSecondsRealtime(delay);

            BackgroundIMG.gameObject.transform.DOScale(Vector3.zero, fadeTime)
                 .SetEase(Ease.InOutExpo)
                .OnComplete(() =>
                {
                    BackgroundIMG.gameObject.SetActive(false);
                    Debug.Log("Fade Out Complete!");
                });
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
        

    }
    IEnumerator LoadScene(int sceneIndex, bool isSplash = false)
    {
        if (!isSplash)
        {
            float fadeTime = 1f;
            float delay = 1f;

            BackgroundIMG.gameObject.transform.localScale = Vector3.zero;
            BackgroundIMG.gameObject.SetActive(true);
            BackgroundIMG.gameObject.transform.DOScale(Vector3.one, fadeTime)
                .SetEase(Ease.InOutExpo);

            yield return new WaitForSecondsRealtime(delay);

            AsyncOperation asyncLoader = SceneManager.LoadSceneAsync(sceneIndex);

            ProgressTXT.DOColor(_Color.G_DoneGreen, fadeTime);

            while (!asyncLoader.isDone)
            {
                float progress = Mathf.Clamp01(asyncLoader.progress / 0.9f);
                ProgressIMG.fillAmount = progress;
                ProgressTXT.text = "% " + (progress * 100).ToString("###");
                yield return null;
            }
            yield return new WaitForSecondsRealtime(delay);

            BackgroundIMG.gameObject.transform.DOScale(Vector3.zero, fadeTime)
                 .SetEase(Ease.InOutExpo)
                .OnComplete(() =>
                {
                    BackgroundIMG.gameObject.SetActive(false);
                    Debug.Log("Fade Out Complete!");
                });
        }
        else
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }



    void Update()
    {
        GoBackByEscapeButton();
    }

}//EndCalssss

//AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
//activity.Call<bool>("moveTaskToBack", true);