﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using UnityEngine;

public class Database : Singleton<Database>
{


    protected override void Awake()
    {
        base.Awake();

    }





    // Removes the default file extension from path.
    static string RemoveFileExtension(string path, string extention = ".json")
    {
        if (path.Length >= extention.Length)
        {
            //If file extension exist, remove it.
            if (path.ToLower().Substring(path.Length - extention.Length, extention.Length) == extention.ToLower())
                return path.Substring(0, path.Length - extention.Length);
            //File extension doesn't exist.
            else
                return path;
        }
        //Path isn't long enough to contain file extension.
        else
        {
            return path;
        }
    }

    // Removes the directory separator if at the begining of path.
    static string RemoveLeadingDirectorySeparator(string path)
    {
        //Remove directory separate character if it exist on the first character.
        if (char.Parse(path.Substring(0, 1)) == Path.DirectorySeparatorChar || char.Parse(path.Substring(0, 1)) == Path.AltDirectorySeparatorChar)
            return path.Substring(1);
        else
            return path;
    }

    // Returns string result of a text file from Resources.
    static string ReturnFileResource(string path)
    {
        //Remove default file extension and format the path to the platform.
        path = RemoveFileExtension(path);
        path = RemoveLeadingDirectorySeparator(path);

        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return string.Empty;
        }

        //Try to load text from file path.
        TextAsset textAsset = Resources.Load(path) as TextAsset;

        if (textAsset != null)
            return textAsset.text;
        else
            return string.Empty;
    }


    // Returns a database at the file path.
    T ReturnDatabase<T>(string path)
    {
        string result = ReturnFileResource(path);
        Debug.LogWarning("ReturnDatabase -> result text is empty.");
        if (result.Length != 0)
        {
            return JsonConvert.DeserializeObject<T>(result);
        }
        else
        {
            Debug.LogWarning("ReturnDatabase -> result text is empty.");
            return default(T);
        }
    }

    // Returns a database at the file path.
    T DeserializeDatabase<T>(string textAssetAsAPI)
    {
        if (textAssetAsAPI.Length != 0)
        {
            return JsonConvert.DeserializeObject<T>(textAssetAsAPI);
        }
        else
        {
            Debug.LogWarning("ReturnDatabase -> result text is empty.");
            return default(T);
        }
    }

    // Returns a database at the file path.
    List<T> ReturnListDatabase<T>(string path)
    {
        string result = ReturnFileResource(path);
        if (result.Length != 0)
        {
            return JsonConvert.DeserializeObject<List<T>>(result).ToList();
        }
        else
        {
            Debug.LogWarning("ReturnDatabase -> result text is empty.");
            return new List<T>();
        }
    }


}//EndClassss
