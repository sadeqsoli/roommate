﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HelperWebRequestHandler : Singleton<HelperWebRequestHandler>
{

    static AudioClip audioClip;

    public void SendWebCoroutine(string url, HttpMethods method, Action<Response> responseEvent = null, WWWForm form = null, string BodyData = null,
                                            Dictionary<string, string> headers = null)
    {
        HelperWebRequestHandler.DoCoroutine(SendWebRequest(url, HttpMethods.get, responseEvent, form, BodyData, headers));
    }
    





    static void DoCoroutine(IEnumerator coroutine)//this will launch the coroutine on our Instance.
    {
        Instance.StartCoroutine(Instance.Perform(coroutine)); 
    }

    IEnumerator LoadVoice1(string localUrl, AudioSource audioSource)
    {
        AudioClip clip = null;
        UnityWebRequest request = UnityWebRequestMultimedia.GetAudioClip("file://" + localUrl, AudioType.MPEG);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            audioClip = clip = DownloadHandlerAudioClip.GetContent(request);
            audioSource.PlayOneShot(clip);
        }
    }

    IEnumerator Perform(IEnumerator coroutine)
    {
        yield return StartCoroutine(coroutine);
    }
    IEnumerator SendWebRequest(string url, HttpMethods method, Action<Response> responseEvent = null, WWWForm form = null, string BodyData = null, Dictionary<string, string> headers = null)
    {

        UnityWebRequest www = null;
        switch (method)
        {
            case HttpMethods.get:
                www = UnityWebRequest.Get(url);
                break;
            case HttpMethods.post:
                www = UnityWebRequest.Post(url, form);
                break;
            case HttpMethods.delete:
                www = UnityWebRequest.Delete(url);
                break;
            case HttpMethods.head:
                www = UnityWebRequest.Head(url);
                break;
            case HttpMethods.put:
                www = UnityWebRequest.Put(url, BodyData);
                break;
        }
        if (headers != null)
        {
            foreach (var item in headers)
            {
                www.SetRequestHeader(item.Key, item.Value);
            }
        }

        yield return www.SendWebRequest();

        Response r = new Response(www.downloadHandler.text, www.GetResponseHeaders(), www.responseCode, www.error, www.isNetworkError);
        responseEvent.Invoke(r);
        www.Dispose();
    }


}//EndClasssss(HelperWebRequestHandler)






public enum HttpMethods
{
    get, post, delete, head, put
}

public class Response
{
    public Response(string Message, Dictionary<string, string> ResponseHeaders, long MessageCode, string ErrorBody, bool IsError)
    {
        isError = IsError;
        message = Message;
        responseHeaders = ResponseHeaders;
        messageCode = MessageCode;
        errorBody = ErrorBody;
    }

    public string message;
    public Dictionary<string, string> responseHeaders;
    public long messageCode;
    public string errorBody;
    public bool isError;
}