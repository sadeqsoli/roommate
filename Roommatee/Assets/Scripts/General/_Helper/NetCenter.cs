﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;

public class NetCenter : Singleton<NetCenter>
{


    public T[] GetBackAsArrays<T>(string reqURL)
    {
        StartCoroutine(GetRequest(reqURL, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                T[] typeOfSomethingAsArray = JsonConvert.DeserializeObject<T[]>(req.downloadHandler.text);

                foreach (T singleType in typeOfSomethingAsArray)
                {
                    //do Something
                }
            }
        }));
        return default;
    }

    public List<T> GetBackAsList<T>(string reqURL)
    {
        StartCoroutine(GetRequest(reqURL, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                List<T> typeOfSomethingAsList = JsonConvert.DeserializeObject<List<T>>(req.downloadHandler.text);

                foreach (T singleType in typeOfSomethingAsList)
                {
                    //do Something
                }
            }
        }));
        return default;
    }

    public T GetBackAsSingleType<T>(string reqURL)
    {
        StartCoroutine(GetRequest(reqURL, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                T typeOfSomething = JsonConvert.DeserializeObject<T>(req.downloadHandler.text);


                //do Something with result => in this case typeOfSomething

            }
        }));
        return default;
    }

    //For Downloading to persistent data
    public void DownloadImageToDisk(string Serverurl, string localUrl)
    {
        StartCoroutine(ImageRequest(Serverurl, (UnityWebRequest req) =>
       {
           if (req.isNetworkError || req.isHttpError)
           {
               Debug.Log($"{req.error}: {req.downloadHandler.text}");
           }
           else
           {
               if (!Directory.Exists(localUrl))
               {
                   Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
               }
               File.WriteAllBytes(localUrl, req.downloadHandler.data);
           }
       }));
    }

    public void DownloadSoundToDisk(string Serverurl, string localUrl, AudioType audioType = AudioType.MPEG)
    {
        StartCoroutine(SoundRequest(Serverurl, audioType, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                if (!Directory.Exists(localUrl))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
                }
                File.WriteAllBytes(localUrl, req.downloadHandler.data);
            }
        }));
    }






    //For UI Systems
    public void DownloadVideoFromDisk(string ServerUrl, Image image)
    {
        StartCoroutine(ImageRequest("file://" + ServerUrl, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                // Get the texture out using a helper downloadhandler
                Texture2D texture = DownloadHandlerTexture.GetContent(req);
                // Save it into the Image UI's sprite
                Sprite sprite = TexToSprite.ConvertToSprite(texture);
                image.sprite = sprite;
            }
        }));
    }


    //For UI Systems
    public void DownloadImageFromDisk(string ServerUrl, Image image)
    {
        StartCoroutine(ImageRequest("file://" + ServerUrl, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                // Get the texture out using a helper downloadhandler
                Texture2D texture = DownloadHandlerTexture.GetContent(req);
                // Save it into the Image UI's sprite
                Sprite sprite = TexToSprite.ConvertToSprite(texture);
                image.sprite = sprite;
            }
        }));
    }

    //For Sprite Renderer
    public void DownloadImageFromDisk(string ServerUrl, SpriteRenderer spriteRenderer, float ppu = 190f)
    {
        StartCoroutine(ImageRequest("file://" + ServerUrl, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                // Get the texture out using a helper downloadhandler
                Texture2D texture = DownloadHandlerTexture.GetContent(req);
                // Save it into the Image UI's sprite
                Sprite sprite = TexToSprite.ConvertToSprite(texture, ppu);
                if (sprite != null)
                    spriteRenderer.sprite = sprite;
            }
        }));
    }



    //Returning A Sprite or sound after download.
    public void DownloadImage(string Url, UnityAction<Sprite> unityAction_SP)
    {
        StartCoroutine(ImageRequest("file://" + Url, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                // Get the texture out using a helper downloadhandler
                Texture2D texture = DownloadHandlerTexture.GetContent(req);
                // Save it into the Image UI's sprite
                Sprite sprite = TexToSprite.ConvertToSprite(texture);
                if (sprite != null)
                    unityAction_SP?.Invoke(sprite);
            }
        }));
    }
    public void DownloadSound(string ServerUrl, UnityAction<AudioClip> unityAction_AC, AudioType audioType = AudioType.MPEG)
    {
        StartCoroutine(SoundRequest("file://" + ServerUrl, audioType, (UnityWebRequest req) =>
         {
             if (req.isNetworkError || req.isHttpError)
             {
                 Debug.Log($"{req.error}: {req.downloadHandler.text}");
             }
             else
             {
                 // Get the sound out using a helper class
                 AudioClip clip = DownloadHandlerAudioClip.GetContent(req);
                 // Load the clip into our audio source and play
                 if (clip != null)
                     unityAction_AC?.Invoke(clip);
             }
         }));
    }
   

    //Test 
    public IEnumerator GETAPI(string baseURL, string api, string authToken)
    {
        string url = Path.Combine(baseURL, api);
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SetRequestHeader("Authorization", "Bearer " + authToken);
        yield return www.SendWebRequest();

        if (www.error == null)
        {
            Debug.Log("Succesful");
        }
        else
        {
            Debug.Log("ERROR: " + www.error);
        }
    }




    IEnumerator SoundRequest(string ServerUrl, AudioType audioType, Action<UnityWebRequest> soundCallback)
    {
        // Note, we try to download an OGGVORBIS (ogg) file because Windows doesn't support
        // MPEG readily. If you're on a mac, you can try MPEG (mp3)
        using (UnityWebRequest req = UnityWebRequestMultimedia.GetAudioClip(ServerUrl, audioType))
        {
            yield return req.SendWebRequest();
            soundCallback(req);
        }
    }
    IEnumerator ImageRequest(string ServerUrl, Action<UnityWebRequest> imageCallback)
    {
        using (UnityWebRequest req = UnityWebRequestTexture.GetTexture(ServerUrl))
        {
            yield return req.SendWebRequest();
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }
            imageCallback(req);
        }
    }
    IEnumerator VideoRequest(string ServerUrl, Action<UnityWebRequest> imageCallback)
    {
        using (UnityWebRequest req = UnityWebRequestTexture.GetTexture(ServerUrl))
        {
            yield return req.SendWebRequest();
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }
            imageCallback(req);
        }
    }
    IEnumerator GetRequest(string ServerUrl, Action<UnityWebRequest> apiCallback)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(ServerUrl))
        {
            // Send the request and wait for a response
            yield return request.SendWebRequest();

            apiCallback(request);
        }
    }





}//EndClassss
