﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserData
{
    public UserIdentity UserIdentity { get; set; }
    public UserSettings UserSettings { get; set; }
    public UserState UserState { get; set; }
}
[System.Serializable]
public struct UserIdentity
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Aka { get; set; }
    public string PhoneNumber { get; set; }
    public int Age { get; set; }
    public Gender Gender { get; set; }
    public Nationality UserNationality { get; set; }
    public string Location { get; set; }
    public float Weight { get; set; }
    public float Height { get; set; }
    public BodyType CurrentBodyType { get; set; }
}

[System.Serializable]
public class UserSettings
{
    public PracticeHabbits PracticeHabbits { get; set; }
    public EdibleHabbits EdibleHabbits { get; set; }
    public Perferences Perferences { get; set; }

}

[System.Serializable]
public class PracticeHabbits
{
    public int[] PracticeDays { get; set; } = new int[7];
    public DateTime PracticeTime { get; set; } 
    public int HowMuch { get; set; } 
    public string PracticePlace { get; set; } 
}

[System.Serializable]
public class EdibleHabbits
{
    public int[] EatingDays { get; set; } = new int[7];
    public DateTime[] EatingTime { get; set; } = new DateTime[7];
    public int HowMuch { get; set; } 
    public string EatingPlace { get; set; } 
}
[System.Serializable]
public struct Perferences
{
    public BodyType IdealBodyType { get; set; } 
    public float IdealWeight { get; set; } 
    //By Month
    public int IdealTime { get; set; } 
    public int IdealWorkoutTime { get; set; }
}


[System.Serializable]
public struct UserState
{
    public int Score { get { return ScoreRepo.GetScore(); } set { ScoreRepo.SetScore(value); } }
    public int Coin { get { return CoinRepo.GetCoin(); } set { CoinRepo.PushCoins(value); } }
    public int PlayerLevel { get { return ScoreRepo.GetScore() / 1000; } }
    public int PlayerXP { get { return ScoreRepo.GetScore() / 1000; } }

}







