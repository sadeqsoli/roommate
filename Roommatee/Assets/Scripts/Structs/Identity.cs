﻿
public struct Identity 
{
    public Nationality MostlyServed { get; set; }
    public Nationality OriginalNationality { get; set; }
    
}
