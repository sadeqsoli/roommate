﻿
public struct Ingredient 
{
    public string Name { get; set; }
    public float Amount { get; set; }
    public AmountType AmountType { get; set; }
    public TemperType TemperType { get; set; }
    public PhysiqueType PhysiqueType { get; set; }
}
