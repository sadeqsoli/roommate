﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Prepration 
{
    public int ServingTime { get; set; }
    public int CookingTime { get; set; }
    public DifficultyLevel CookingLevel { get; set; }
    public string HowTo { get; set; }

}
