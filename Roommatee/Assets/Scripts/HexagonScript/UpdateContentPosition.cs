﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateContentPosition : MonoBehaviour
{
    void OnEnable()
    {
        RectTransform rectTR = GetComponent<RectTransform>() ;
        rectTR.localPosition = new Vector3(0, -50, rectTR.localPosition.z);
    }

}
