﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class ExitStruct : MonoBehaviour
{
    [SerializeField] Button ContinueButton, ExitButton;

    Animator anim;


    private void Start()
    {
        anim = GetComponent<Animator>();
        ContinueButton.onClick.AddListener(FadeOut);
    }


    public void ExitFromProcess(UnityAction unityAction)
    {
        ExitButton.onClick.RemoveAllListeners();
        ExitButton.onClick.AddListener(unityAction);
        ExitButton.onClick.AddListener(FadeOut);
    }

    void FadeOut()
    {
        anim.SetBool("fadeout", true);
    }
    void FadeOutOff()
    {
        anim.SetBool("fadeout", false);
        gameObject.SetActive(false);
    }
}
