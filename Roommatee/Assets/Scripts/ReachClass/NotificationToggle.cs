﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationToggle : MonoBehaviour
{
    Toggle notifToggle;
    Animator animator;


    void Start()
    {
        animator = GetComponent<Animator>();
        notifToggle = GetComponent<Toggle>();
        notifToggle.onValueChanged.AddListener(SetButtonOFFAndON);
    }

    void SetButtonOFFAndON(bool isOn)
    {
        animator.SetBool("NotifOn", isOn);
    }
}
