﻿using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.Events;

public class ShopManager : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields

    [SerializeField] Button GoPremiumButton;
    [SerializeField] Button[] products;

    [SerializeField] Image MostPopularIMG;
    [SerializeField] Sprite BlueMostPopularSprites;
    [SerializeField] Sprite WhiteMostPopularSprites;

    [Header("Button Sprite")]
    [SerializeField] Sprite DeselectedProductSprites;
    [SerializeField] Sprite SelectedProductSprites;



    readonly string[] ProductID =
        {
        "3MSsa7sf43sa948fg5734",
        "6MS9s454dfdgfg42fdfd353" ,
        "12MS9d3ss7sd439dfd485fgf4"
    };

    readonly string[] ProductDescribtion =
        {
        "3MonthSubscribtion",
       "6MonthSubscribtion" ,
        "12MonthSubscribtion"
    };
    readonly int[] ProductPrice = { 3000, 6000, 12000 };

    #endregion

    #region Public Methods





    void Purchase(int price, string desc, string productID)
    {
        Zarinpal.Purchase(price, desc, productID);
    }



    #endregion


    #region Private Methods
    void Awake()
    {
        Zarinpal.Initialize();
    }

    void Start()
    {
        InitializeShelf();

        Zarinpal.StoreInitialized += Zarinpal_StoreInitialized;
        Zarinpal.StoreInitializeFailed += Zarinpal_StoreInitializeFailed;
        Zarinpal.PurchaseStarted += Zarinpal_PurchaseStarted;
        Zarinpal.PurchaseFailedToStart += Zarinpal_PurchaseFailedToStart;
        Zarinpal.PurchaseSucceed += Zarinpal_PurchaseSucceed;
        Zarinpal.PurchaseFailed += Zarinpal_PurchaseFailed;
        Zarinpal.PurchaseCanceled += Zarinpal_PurchaseCanceled;
        Zarinpal.PaymentVerificationStarted += Zarinpal_PaymentVerificationStarted;
        Zarinpal.PaymentVerificationSucceed += Zarinpal_PaymentVerificationSucceed;
        Zarinpal.PaymentVerificationFailed += Zarinpal_PaymentVerificationFailed;
    }

    void OnDestroy()
    {
        Zarinpal.StoreInitialized -= Zarinpal_StoreInitialized;
        Zarinpal.StoreInitializeFailed -= Zarinpal_StoreInitializeFailed;
        Zarinpal.PurchaseStarted -= Zarinpal_PurchaseStarted;
        Zarinpal.PurchaseFailedToStart -= Zarinpal_PurchaseFailedToStart;
        Zarinpal.PurchaseSucceed -= Zarinpal_PurchaseSucceed;
        Zarinpal.PurchaseFailed -= Zarinpal_PurchaseFailed;
        Zarinpal.PurchaseCanceled -= Zarinpal_PurchaseCanceled;
        Zarinpal.PaymentVerificationStarted -= Zarinpal_PaymentVerificationStarted;
        Zarinpal.PaymentVerificationSucceed -= Zarinpal_PaymentVerificationSucceed;
        Zarinpal.PaymentVerificationFailed -= Zarinpal_PaymentVerificationFailed;
    }



    void InitializeShelf()
    {
        SelectProduct(2);
        products[0].onClick.AddListener(delegate { SelectProduct(0); });
        products[1].onClick.AddListener(delegate { SelectProduct(1); });
        products[2].onClick.AddListener(delegate { SelectProduct(2); });
    }


    void SelectProduct(int numb)
    {
        MostPopularIMG.sprite = numb == 2 ? WhiteMostPopularSprites : BlueMostPopularSprites;
        for (int i = 0; i < products.Length; i++)
        {
            if (numb == i)
            {
                int price = ProductPrice[i];
                string productDes = ProductDescribtion[i];
                string productID = ProductID[i];
                ChangeListener(delegate { Purchase(price, productDes, productID); });
                products[i].GetComponent<Image>().sprite = SelectedProductSprites;
            }
            else
            {
                products[i].GetComponent<Image>().sprite = DeselectedProductSprites;
            }
        }
    }
    void ChangeListener(UnityAction unityAction)
    {
        GoPremiumButton.onClick.RemoveAllListeners();
        GoPremiumButton.onClick.AddListener(unityAction);
    }






    void Zarinpal_StoreInitialized()
    {
        Log("Store initialized");
    }

    void Zarinpal_StoreInitializeFailed(string error)
    {
        LogError(error);
    }

    void Zarinpal_PurchaseStarted()
    {
        Log("Purchase started");
    }

    void Zarinpal_PurchaseFailedToStart(string error)
    {
        LogError("Purchase failed to start : " + error);
    }

    void Zarinpal_PurchaseSucceed(string productID, string authority)
    {
        Log(string.Format("Purchase success : productID : {0} , authority : {1} ", productID, authority));
    }

    void Zarinpal_PurchaseFailed()
    {
        LogError("Purchase failed");
    }

    void Zarinpal_PurchaseCanceled()
    {
        Log("Purchase canceled by user");
    }

    void Zarinpal_PaymentVerificationStarted(string authority)
    {
        Log("Start verifying purchase for : url : " + authority);
    }

    void Zarinpal_PaymentVerificationSucceed(string refID)
    {
        Log("Purchase verification success : refid : " + refID);
    }

    void Zarinpal_PaymentVerificationFailed()
    {
        LogError("Purchase verification failed");
    }

    void Log(string log)
    {
        Debug.Log(DateTime.Now.ToLongTimeString()+ log);
        //purchaseLogTXT.text += "\n" + DateTime.Now.ToLongTimeString() + "  : <color=#FFFFFFFF>" + log + "</color>";
    }

    void LogError(string error)
    {
        Debug.Log(DateTime.Now.ToLongTimeString() + error);
        //purchaseLogTXT.text += "\n" + DateTime.Now.ToLongTimeString() + "  : <color=#FF0000FF>" + error + "</color>";
    }
    void Shopping(int amount, string desc, string productID)
    {
        Zarinpal.Purchase(amount, desc, productID);
    }

    void PurchaseCallback()
    {
        Zarinpal.PurchaseSucceed += (string productID, string authority) =>
        {
            Debug.Log("purchase succeed with authority : " + authority);
        };
    }




    #endregion
}//EndClasssss
