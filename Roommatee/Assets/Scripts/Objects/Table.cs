﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    public string Id { get; set; }
    public string Name { get; set; }
    //---- >>Also known as...
    public string Aka { get; set; }
    public int Rating { get; set; }
    public Prepration TablePrepration { get; set; }
    public Dish[] Dishes { get; set; }
    public Tag[] Tags { get; set; }
}
