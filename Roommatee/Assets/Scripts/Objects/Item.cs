﻿
public class Item 
{
    public string Id { get; set; }
    public string Name { get; set; }
    //Also known as.../
    public string Aka { get; set; }
    public PhysiqueType PhysiqueType { get; set; }
    public int WasGood { get; set; }
    public int Price { get; set; }

}
