﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : Edible
{
    public FoodType FoodType { get; set; }
    public Prepration FoodPrepration { get; set; }
    public Ingredient[] Ingredients { get; set; }

}
