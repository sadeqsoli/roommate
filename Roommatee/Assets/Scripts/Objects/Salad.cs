﻿
public class Salad : Edible
{
    public FoodType FoodType { get; set; }
    public Prepration TablePrepration { get; set; }
    public Ingredient[] Ingredients { get; set; }
}
