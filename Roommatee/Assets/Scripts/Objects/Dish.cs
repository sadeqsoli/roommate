﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dish 
{
    public string Id { get; set; }
    public string Name { get; set; }
    //---- >>Also known as...
    public string Aka { get; set; }
    public Identity Identity { get; set; }
    public Prepration DishPrepration { get; set; }
    public Food[] Foods { get; set; }
    public Salad[] Salads { get; set; }
    public Drink[] Drinks { get; set; }
    public Tag[] Tags { get; set; }

}
