﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edible 
{
    public string Id { get; set; }
    public string Name { get; set; }
    //Also known as.../
    public string Aka { get; set; }
    public TemperType TemperType { get; set; }
    public PhysiqueType PhysiqueType { get; set; }
    public DigestiveType DigestiveType { get; set; }
    public int WasGood { get; set; }
    public Tag[] Tags { get; set; }

}
