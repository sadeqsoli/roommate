﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drink : Edible
{
    public DrinkType DrinkType { get; set; }
    public Prepration DrinkPrepration { get; set; }
    public Ingredient[] Ingredients { get; set; }
}
